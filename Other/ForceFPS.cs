using UnityEngine;

// SEE THE NOTE IN THE README file before using this....

// this script is from GenLockProofOfConcept
// Toulouse de Margerie
// ToulouseUnity
// https://github.com/Unity-Technologies/GenLockProofOfConcept/blob/master/Assets/ForceRenderRate.cs

public class ForceFPS : MonoBehaviour {
    public float targetFPS = 12.0f;
    private float currentFrameTime;

    private void Start() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 9999;
        currentFrameTime = Time.realtimeSinceStartup;
        StartCoroutine("WaitForNextFrame");
    }

    private System.Collections.IEnumerator WaitForNextFrame() {
        while(true) {
            targetFPS = Mathf.Max(targetFPS, .1f);

            yield return new WaitForEndOfFrame();
            currentFrameTime += 1.0f / targetFPS;
            float t = Time.realtimeSinceStartup;
            float sleepTime = currentFrameTime - t - 0.01f;
            if(sleepTime > 0) {
                System.Threading.Thread.Sleep((int) (sleepTime * 1000));
            }

            while(t < currentFrameTime) {
                t = Time.realtimeSinceStartup;
            }
        }
    }
}
