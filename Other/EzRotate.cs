﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


// this class shows up to call update function 
// in a way that only updates if the animation frame has updated.
public class EzRotate : EZAnimatorMonoBehavior {
    public Vector3 rotate = new Vector3(0, 180, 0);

    protected override void OnFrameUpdate(float deltaTime) {
        transform.rotation *= Quaternion.Euler(rotate * deltaTime);
    }

    protected override void OnStateUpdate(string newStateName) {
        // do nothing
    }
}
