using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[System.Serializable]
public class EzTextureAnimator : EZAnimatorStateBase<Texture2D> {
    public override string GetSpriteOrTextureName(int index) {
        if(_frames.Count < index && index >= 0) {
            Texture2D f = _frames[index];
            if(f != null) {
                return f.name;
            }
        }

        return null;
    }
}
