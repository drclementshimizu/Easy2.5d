Easy 2.5d  
By Dr. Clement Shimizu
Instagram: @drclementshimizu
Web: http://www.clementshimizu.com/
Youtube: https://www.youtube.com/user/ClementShimizu/
  
What is 2.5d?
Two-and-a-half-dimensional graphics is 2D art living in 3D world!  This example is 2.5D test game by Dr. Clement Shimizu  
and Canadian animator Paloma Dawkins.  Placing 2D sprites into a 3D scene creates a strong illusion of depth.
![alt text](docs/.swiming-test-clementshimizu-palgal.gif "This is an example of 2.5D animation by Dr. Clement Shimizu and animator Paloma Dawkins.")

Q: What are the licence terms?
A: Educational use only. All other uses please contact me.

Q: What is the best way to add this as a project?
A: You can download the code files from here as a zip format, but git users will benefit from cloning as a submodule.

```
git submodule add git@gitlab.com:drclementshimizu/Easy2.5d.git Assets/Easy25D
git add .gitmodules
git commit -m "Added https://gitlab.com/drclementshimizu/Easy2.5d"
```

This will allow you to stay UP TO DATE like a pro!  Just be careful when updating because its still in alpha and things  
will break/change during this phase of development.

Q: How can I use this if I don't know how to write CS code!  
A: PLEASE check out the example project!  I wrote a hand full of unity3d components that allow a non-coder to trigger  
   animations by proximity, bumping into objects, or clicking on objects.

```
https://gitlab.com/drclementshimizu/Easy-2.5d-Example-Unity3d-Project
```

Q: Is this compatible with HDRP, LWRP, VFX?  
A: Yes, I tested it and it works great.  The sample scene was built for the legacy pipeline.  

Q: Hey my animation looks good when it is still or when I am moving, but it jitters when it moves.  
A: This may be because the sprite is animating at a different rate then the game!  
   While the nuclear option is to run the whole game at one frame rate (ie 12fps game 12fps animations),  
   it is more practical run the game at an unconstrained frame rate, but move sprites ONLY when the frame is being updated.   
   EasyRotate.cs shows an example on how to do this correctly.  
  
Q: Its a pain to reorder sprites in the frame list.  What if i want to rearrange frames!  
A: Some unity plugins allow you to reorder lists.  My highest recommendations go to Odin Inspector.  

Q: When I try to integrate this with other scripts they jitter even though my frame rate is high?  
A: Most likely your animation is a say 12 frames per second but you are moving it around at a different frame rate!  
   What happens is the frame animation updates, moves a few frames on the screen then updates again.  This causes your
   eye to have strain!  The solution is to move things it around ONLY when the frame is being updated.
   The animation class exposes a callback when the frames are updated.  It builds up the Delta Time too and releases 
   when the frame updates so animation is precise.
   
   EzRotate.cs shows an example on how to slave your scripts to the animation correctly.
   
   One exception is bill-boarding which should be done at the last possible moment every frame.
   Which if you use the bill-boarding option it will apply it at the correct phase.

Q: I want to run my game at 12 or 24 frames per second.
A: The absolute most precise method of running the animations is to lock the game's refresh rate to the frame rate
   the animator works in.
    
   Unity3D exposes a variable called Application.targetFrameRate which sounds like it will allow you to run custom frame
   rates but that does't work!  
   
   The real solution can be seen in ForceFPS.cs which uses the example from Unity's GenLockProofOfConcept project.  If
   you animate at 12 fps you can set the frame rate of the game to be 12 with that script.  I believe the
   nVidia g-sync compatible displays will allow you run games at weird refresh rates and get perfect vSync, but I 
   haven't tested it yet.
   
   THAT BEING SAID: I didn't enable this script by default because I think most people would prefer the flexibility
   of running animations at variable playback rates.

Other reading/Watching:  
  
You can see a game that we made with 2.5D animation here.  It is using the more extreme internal version of the 2.5D  
Animation system called "Now That's What I Call 2.5D".  You will not be able to get all of the effects but you can get  
most of them without coding using Easy25d.  

https://www.youtube.com/watch?v=2ZkJlXt8Guw