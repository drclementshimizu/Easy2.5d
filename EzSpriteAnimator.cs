using System.Collections.Generic;
using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzSpriteAnimator : EzAnimator<Sprite, EzAnimatorStateSprite> {
    protected UnityEngine.UI.Image targetUIImage;
    protected SpriteRenderer targetSpriteRenderer;

    private void Awake() {
        killAllSprites();
        targetSpriteRenderer = GetComponent<SpriteRenderer>();
        targetUIImage = GetComponent<UnityEngine.UI.Image>();
    }

    private void Start() {
        Resize(true);
        if(spriteOptions.billboardOptions.type == SpriteOptions.BillboardOptions.BILLBOARDTYPE.CopyCamera
        ) {
            spriteOptions.billboardOptions.billboardTarget = Camera.main ? Camera.main.transform : null;
        }
    }

    public void BootstrapDepthSlicer() {
        int frameIndex = (int) currentTimeFrames;
        for(int i = 0; i < depthSlicerSprites.Count; i++) {
            switch(spriteOptions.depthSlicerOptions.advancedOptions.frameOffset) {
                case DepthSlicerOptions.OFFSETMODE.ONE:
                    frameIndex += 1;
                    break;
                case DepthSlicerOptions.OFFSETMODE.NONE:
                    break;
                case DepthSlicerOptions.OFFSETMODE.NEGATVIEONE:
                    frameIndex -= 1;
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }

            depthSlicerSprites[i].sprite = currentState.GetFrame(frameIndex);
        }
    }

    protected override void ApplyFrame(Sprite frame, EzAnimatorStateSprite state) {
        //        Debug.Log("Current time o1;"+currentTime01);
        //      Debug.Log("Current time f;"+currentTimeFrames + " / "+ durationFrames);
        base.ApplyFrame(frame, state);

        Color c = spriteOptions.colorOptions.GetColor(this);

        if(targetSpriteRenderer != null) {
            targetSpriteRenderer.sprite = frame;
            targetSpriteRenderer.flipX = state.spriteOptions.flipX;
            targetSpriteRenderer.flipY = state.spriteOptions.flipY;
            targetSpriteRenderer.color = c;
        }

        if(targetUIImage) {
            targetUIImage.sprite = frame;
            targetUIImage.color = c;
        }

        if(Resize() || spriteOptions.depthSlicerOptions.enabled && spriteOptions.depthSlicerOptions.positionDirty) {
         
            spriteOptions.depthSlicerOptions.ApplyDepthSlicer(this);
            //            spriteOptions.depthSlicerOptions.  ApplyFrames(this);
        }

        if(spriteOptions.depthSlicerOptions.enabled) {
            spriteOptions.depthSlicerOptions.ApplyFrames(this);
            if(spriteOptions.depthSlicerOptions.gradientOptions.enabled) {
                for(int i = 0; i < depthSlicerSprites.Count; i++) {
                    float t = i / (float) (depthSlicerSprites.Count - 1);
                    float offset_01 = 0;
                    switch(spriteOptions.depthSlicerOptions.advancedOptions.frameOffset) {
                        case DepthSlicerOptions.OFFSETMODE.ONE:
                            if(durationFrames > 1) {
                                offset_01 -= i / (float) (durationFrames - 1);
                            }

                            break;
                        case DepthSlicerOptions.OFFSETMODE.NEGATVIEONE:
                            if(durationFrames > 1) {
                                offset_01 += i / (float) (durationFrames - 1);
                            }

                            break;
                        case DepthSlicerOptions.OFFSETMODE.NONE:
                            break;
                        default:
                            throw new System.ArgumentOutOfRangeException();
                    }

                    Color c1 = spriteOptions.colorOptions.GetColor(this, offset_01, t);
                    Color c2 = spriteOptions.depthSlicerOptions.gradientOptions.depthGradient.Evaluate(t);
                    depthSlicerSprites[i].color = c1 * c2;
                }
            }
        }
    }


    public SpriteOptions spriteOptions = new SpriteOptions();

    private void LateUpdate() {
        if(
            spriteOptions.billboardOptions.type == SpriteOptions.BillboardOptions.BILLBOARDTYPE.CopyCamera ||
            spriteOptions.billboardOptions.type == SpriteOptions.BillboardOptions.BILLBOARDTYPE.Target
        ) {
            if(spriteOptions.billboardOptions.billboardTarget == null) {
                spriteOptions.billboardOptions.billboardTarget = Camera.main ? Camera.main.transform : null;
            }

            if(spriteOptions.billboardOptions.billboardTarget != null) {
                if(spriteOptions.depthSlicerOptions.enabled) {
                    for(int i = 0; i < depthSlicerSprites.Count; i++) {
                        depthSlicerSprites[i].transform.rotation = spriteOptions.billboardOptions.billboardTarget.rotation;
                    }
                } else {
                    transform.rotation = spriteOptions.billboardOptions.billboardTarget.rotation;
                }
            }
        }
    }


    [System.NonSerialized]
    public List<SpriteRenderer> depthSlicerSprites = new List<SpriteRenderer>();

    private void OnDestroy() {
        killAllSprites();
    }

    private void OnDisable() {
        killAllSprites();
    }

    private const string depthSlicerChildName = "Slice";

    public bool Resize(bool forceKill = false) {
        bool needsFrameUpdate = false;
        if(forceKill) {
            killAllSprites();
        }

        if(spriteOptions.depthSlicerOptions.enabled) {
            if(targetSpriteRenderer) {
                targetSpriteRenderer.enabled = false;
            }

            while(spriteOptions.depthSlicerOptions.count != depthSlicerSprites.Count) {
                if(spriteOptions.depthSlicerOptions.count > depthSlicerSprites.Count) {
                    SpriteRenderer s = new GameObject(depthSlicerChildName, typeof(SpriteRenderer)).GetComponent<SpriteRenderer>();
                    s.transform.parent = transform;
                    s.gameObject.hideFlags = spriteOptions.depthSlicerOptions.advancedOptions.hideSlices ? HideFlags.HideAndDontSave : HideFlags.DontSave;
                    depthSlicerSprites.Add(s);
                    needsFrameUpdate = true;
                    int frameOffsetIndex = depthSlicerSprites.Count - 1;
                    switch(spriteOptions.depthSlicerOptions.advancedOptions.frameOffset) {
                        case DepthSlicerOptions.OFFSETMODE.ONE:
                     
                            break;
                        case DepthSlicerOptions.OFFSETMODE.NEGATVIEONE:
                            frameOffsetIndex = -frameOffsetIndex;

                            break;
                        case DepthSlicerOptions.OFFSETMODE.NONE:
                            frameOffsetIndex = 0;
                            break;
                        default:
                            throw new System.ArgumentOutOfRangeException();
                    }

               s.sprite=     currentState.GetFrame((int) currentTimeFrames + frameOffsetIndex);
                } else {
                    if(Application.isPlaying) {
                        Destroy(depthSlicerSprites[0].gameObject);
                    } else {
                        DestroyImmediate(depthSlicerSprites[0].gameObject);
                    }

                    depthSlicerSprites.RemoveAt(0);
                }
            }

            if(spriteOptions.depthSlicerOptions.advancedOptions.hideSlices != spriteOptions.depthSlicerOptions.wasHidden) {
                spriteOptions.depthSlicerOptions.wasHidden = spriteOptions.depthSlicerOptions.advancedOptions.hideSlices;
                for(int i = 0; i < depthSlicerSprites.Count; i++) {
                    depthSlicerSprites[i].gameObject.hideFlags = spriteOptions.depthSlicerOptions.advancedOptions.hideSlices ? HideFlags.HideAndDontSave : HideFlags.DontSave;
                }
            }
        } else {
            if(depthSlicerSprites.Count > 0) {
                killAllSprites();
            }
        }

        return needsFrameUpdate;
    }


    private void killAllSprites() {
        // WE REALLY WANT TO KILL EVERYTHING!!!
        // if the player duplicates the depth slicer it is possible that we get duplicated 
        for(int i = 0; i < transform.childCount; i++) {
            SpriteRenderer child = transform.GetChild(i).GetComponent<SpriteRenderer>();
            if(child && child.name == depthSlicerChildName) {
                depthSlicerSprites.Add(child);
            }
        }

        // now delete everything and be careful of duplicates!
        foreach(SpriteRenderer VARIABLE in depthSlicerSprites) {
            if(VARIABLE && VARIABLE.gameObject) {
                if(Application.isPlaying) {
                    Destroy(VARIABLE.gameObject);
                } else {
                    DestroyImmediate(VARIABLE.gameObject);
                }
            }
        }

        depthSlicerSprites.Clear();
    }


    [System.Serializable]
    public class DepthSlicerOptions {
        [ConditionalHide(nameof(isInAnimationEditor))]
        public bool enabled = false;

        public virtual bool isInAnimationEditor {
            get { return false; }
        }


        public virtual bool showTransformOptions {
            get { return enabled || isInAnimationEditor; }
        }

        public virtual bool showColorOptions {
            get { return enabled && !isInAnimationEditor; }
        }

        [System.NonSerialized]
        public bool wasHidden = true;


        public int count {
            get { return Mathf.Clamp(advancedOptions.count, 3, 10000); }
        }

        public enum OFFSETMODE {
            ONE = 0,
            NEGATVIEONE = 1,
            NONE = 2
        }

        [System.NonSerialized]
        public bool positionDirty = true;

        [ConditionalShow(nameof(showTransformOptions))]
        public TransformInfo pivot = new TransformInfo();

        [ConditionalShow(nameof(showTransformOptions))]
        public TransformInfo offset = new TransformInfo();


        public void ApplyOptions(DepthSlicerOptions source, bool justAnimatable) {
            pivot.Apply(source.pivot);
            offset.Apply(source.offset);

            advancedOptions.ApplyOptions(source.advancedOptions, justAnimatable);
            if(!justAnimatable) {
                gradientOptions.ApplyOptions(source.gradientOptions);
            }

            positionDirty = true;
        }

        public enum MODE {
            OffsetTimesIndex,
            SpreadBetweenPivotAndOffet,
            OFFSETequalsOFFSETplusLAST,
            OFFSETequalsOFFSETxSINTIME
        }

        [System.Serializable]
        public class TransformInfo {
            public Vector3 localPosition = Vector3.zero;
            public Vector3 localEulerAngles = Vector3.zero;
            public Vector3 localScale = Vector3.one;

            public void Apply(TransformInfo source) {
                localPosition = source.localPosition;
                localEulerAngles = source.localEulerAngles;
                localScale = source.localScale;
            }
        }

        public void ApplyDepthSlicer(EzSpriteAnimator animator) {
            positionDirty = false;
            if(advancedOptions.randomSeed == null) {
                advancedOptions.randomSeed = new RandomReseedable();
            }
            advancedOptions.randomSeed.Reset();
            for(int i = 0; i < animator.depthSlicerSprites.Count; i++) {
                DepthSlicerIndexXOffset.Offset(this, animator.depthSlicerSprites[i], animator, i, animator.depthSlicerSprites.Count);
            }
        }

        public void ApplyFrames(EzSpriteAnimator animator) {
            if(animator.depthSlicerSprites.Count > 3) {
                switch(advancedOptions.frameOffset) {
                    case OFFSETMODE.NEGATVIEONE:
                        for(int i = 0; i < animator.depthSlicerSprites.Count - 1; i++) {
                            CopySprite(animator.depthSlicerSprites[i], animator.depthSlicerSprites[i + 1]);
                        }

                        CopySprite(animator.depthSlicerSprites[animator.depthSlicerSprites.Count - 1], animator.targetSpriteRenderer);
                        break;
                    case OFFSETMODE.ONE:
                        for(int i = animator.depthSlicerSprites.Count - 1; i >= 1; i--) {
                            CopySprite(animator.depthSlicerSprites[i], animator.depthSlicerSprites[i - 1]);
                        }

                        CopySprite(animator.depthSlicerSprites[0], animator.targetSpriteRenderer);
                        break;
                    case OFFSETMODE.NONE:
                        for(int i = 0; i < animator.depthSlicerSprites.Count; i++) {
                            CopySprite(animator.depthSlicerSprites[i], animator.targetSpriteRenderer);
                        }

                        break;
                    default:
                        throw new System.ArgumentOutOfRangeException();
                }
            }
        }

        [System.Serializable]
        public class AdvancedOptions {
            [Header("Number of slices in the depth slicer.  Large numbers will reduce performance, changing this at runtime may cause a hic-up.")]
            public int count = 10;

            public MODE mode = MODE.OffsetTimesIndex;

            [Header("Set this to false if you want to see the individual slices.")]
            public bool hideSlices = true;

            [Header("Controls exact placement of randomized elements.")]
            public RandomReseedable randomSeed;

            public OFFSETMODE frameOffset = OFFSETMODE.ONE;


            public void ApplyOptions(AdvancedOptions advancedOptions, bool justAnimateable) {
                count = advancedOptions.count;
                mode = advancedOptions.mode;
                if(justAnimateable) {
                    frameOffset = advancedOptions.frameOffset;
                    if(randomSeed == null) {
                        randomSeed = new RandomReseedable();
                    }

                    randomSeed.ApplyOptions(advancedOptions.randomSeed);
                    // hide slices ignore.
                }
            }
        }


        [ConditionalShow(nameof(showTransformOptions))]
        public AdvancedOptions advancedOptions = new AdvancedOptions();


        [System.Serializable]
        public class RandomReseedable {
            private ulong randomPlacemarker = (ulong) Random.Range(0, 99999999);
            public ulong seed;

            public RandomReseedable(uint startSeed = 0) {
                Seed(startSeed);
            }

            public void Seed(uint seedToSet = 0) {
                if(seedToSet == 0) {
                    seed = randomPlacemarker = (ulong) Random.Range(0, 999999999);
                } else {
                    seed = randomPlacemarker = seedToSet;
                }
            }

            public void Reset() {
                randomPlacemarker = seed;
            }

            private uint rand(uint max = uint.MaxValue) {
                randomPlacemarker = randomPlacemarker * 1103515245 + 12345;
                return (uint) (randomPlacemarker / 65536) % 32768;
            }

            public float rand01 {
                get { return rand(32768) / (float) (32768 - 1); }
            }

            public float randRange(float min, float max) {
                return min + rand01 * (max - min);
            }


            public void ApplyOptions(RandomReseedable advancedOptions) {
                // randomPlacemarker
                // seed 
            }
        }


        private void CopySprite(SpriteRenderer target, SpriteRenderer example) {
            target.sprite = example.sprite;
            target.flipX = example.flipX;
            target.flipY = example.flipY;
            target.color = example.color;
            target.size = example.size;
            target.tileMode = example.tileMode;
            target.drawMode = example.drawMode;
        }

        [System.Serializable]
        public class GradientOptions {
            public bool enabled = false;

            [Help(@"If enabled, this gradient will be applied to sprites on the depth slicer.
This start of the gradient will tint the first sprite and the end of the gradient will tint the last sprite. 
The tint of the sprite will be combined with the color gradient found in the Color Options gradient.
If enabled, the will cause all of the sprites colors to be updated every frame so experiment with the Color Option’s  mode “loop over time” in combination with positive and negative numbers in Seconds per loop.

Disable this if not needed for performance.")]
            [ConditionalShow(nameof(enabled))]
            public Gradient depthGradient = new Gradient();


            public void ApplyOptions(GradientOptions advancedOptions) {
                enabled = advancedOptions.enabled;
                depthGradient.mode = advancedOptions.depthGradient.mode;
                depthGradient.SetKeys(advancedOptions.depthGradient.colorKeys, advancedOptions.depthGradient.alphaKeys);
            }
        }


        [ConditionalShow(nameof(showColorOptions))]
        public GradientOptions gradientOptions = new GradientOptions();
    }


    [System.Serializable]
    public class SpriteOptions {
        [System.Serializable]
        public class BillboardOptions {
            public enum BILLBOARDTYPE {
                None = 0,
                CopyCamera = 1,
                Target = 2
            }

            public BILLBOARDTYPE type = BILLBOARDTYPE.None;

            private bool _showIfTarget {
                get { return BILLBOARDTYPE.Target == type; }
            }

            [ConditionalShowAttribute(nameof(_showIfTarget))]
            public Transform billboardTarget = null;
        }


        public BillboardOptions billboardOptions = new BillboardOptions();

        public DepthSlicerOptions depthSlicerOptions = new DepthSlicerOptions();

        public ColorOptions colorOptions = new ColorOptions();


        [System.Serializable]
        public class ColorOptions {
            public enum ApplyMode {
                LoopOverAnimationSequence = 0,
                LoopOverTime = 1
            }

            public virtual bool isInAnimationEditor {
                get { return false; }
            }

            [Help(@"The color gradient applied to sprites using multiplicative blending mode.  That means if you have a white sprite and you have a green part of the gradient, the result will be green.  But if you had a red sprite the result will be black.")]
            public Gradient color = new Gradient();

            [ConditionalHide(nameof(isInAnimationEditor))]
            public ApplyMode mode = ApplyMode.LoopOverTime;

            protected bool _showSecondsPerLoop {
                get { return !isInAnimationEditor && mode == ApplyMode.LoopOverTime; }
            }


            [Help(@"This value is how fast the color gradient loops.
If using the depth slicer and gradient options enabled, this will behave differently.
")]
            [ConditionalShow(nameof(_showSecondsPerLoop))]
            public float secondsPerLoop = 1f;

            private float secondsPerLoopSafe {
                get {
                    if(secondsPerLoop * secondsPerLoop < .001f) {
                        return .00001f;
                    }

                    return secondsPerLoop;
                }
            }

            public Color GetColor(EzSpriteAnimator animator) {
                switch(mode) {
                    case ApplyMode.LoopOverAnimationSequence:
                        return color.Evaluate(animator.currentTime01);
                    case ApplyMode.LoopOverTime:
                        return color.Evaluate(Mathf.Repeat(animator.totalTimeSecondsFromAwake / secondsPerLoopSafe, 1));
                    default:
                        throw new System.ArgumentOutOfRangeException();
                }
            }

            public Color GetColor(EzSpriteAnimator animator, float offset01_Animation, float offset01_Depth) {
                switch(mode) {
                    case ApplyMode.LoopOverAnimationSequence:
                        return color.Evaluate(Mathf.Repeat(animator.currentTime01 + offset01_Animation, 1));
                    case ApplyMode.LoopOverTime:
                        return color.Evaluate(Mathf.Repeat(animator.totalTimeSecondsFromAwake / secondsPerLoopSafe + offset01_Depth, 1)); //
                    default:
                        throw new System.ArgumentOutOfRangeException();
                }
            }
        }
    }

    public static class DepthSlicerIndexXOffset {
        public static void Offset(DepthSlicerOptions options, SpriteRenderer ds, EzSpriteAnimator animator, int i, int _count) {
            switch(options.advancedOptions.mode) {
                case DepthSlicerOptions.MODE.OffsetTimesIndex:
                    OffsetIndexTimesOffset(options, ds, animator, i, _count);
                    break;
                case DepthSlicerOptions.MODE.SpreadBetweenPivotAndOffet:
                    Spread(options, ds, animator, i, _count);
                    break;
                case DepthSlicerOptions.MODE.OFFSETequalsOFFSETplusLAST:
                    Offset_Last_plus(options, ds, animator, i, _count);
                    break;
                case DepthSlicerOptions.MODE.OFFSETequalsOFFSETxSINTIME:
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }
        }

        private static Vector3 RandomVectorRangeReseedable(DepthSlicerOptions options, Vector3 min, Vector3 max) {
            return new Vector3(
                options.advancedOptions.randomSeed.randRange(min.x, max.x),
                options.advancedOptions.randomSeed.randRange(min.y, max.y),
                options.advancedOptions.randomSeed.randRange(min.z, max.z)
            );
        }

        public static void Spread(DepthSlicerOptions options, SpriteRenderer ds, EzSpriteAnimator animator, int i, int _count) {
            ds.transform.localPosition = RandomVectorRangeReseedable(options, options.offset.localPosition, options.pivot.localPosition);
            ds.transform.localEulerAngles = RandomVectorRangeReseedable(options, options.offset.localEulerAngles, options.pivot.localEulerAngles);
            ds.transform.localScale =
                Vector3.Lerp(options.offset.localScale, options.pivot.localScale, options.advancedOptions.randomSeed.rand01);
        }

        public static void OffsetIndexTimesOffset(DepthSlicerOptions options, SpriteRenderer ds, EzSpriteAnimator animator, int i, int _count) {
            float t01 = i / (float) _count;

            ds.transform.localPosition = options.pivot.localPosition + options.offset.localPosition * i;
            ds.transform.localEulerAngles = options.pivot.localEulerAngles; // + offset.localEulerAngles * i;
            if(options.offset.localEulerAngles.x != 0) {
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition), new Vector3(1, 0, 0), options.offset.localEulerAngles.x * i);
            }

            if(options.offset.localEulerAngles.y != 0) {
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition), new Vector3(0, 1, 0), options.offset.localEulerAngles.y * i);
            }

            if(options.offset.localEulerAngles.z != 0) {
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition), new Vector3(0, 0, 1), options.offset.localEulerAngles.z * i);
            }

            float x = options.pivot.localScale.x * Mathf.Pow(options.offset.localScale.x, i);
            float y = options.pivot.localScale.y * Mathf.Pow(options.offset.localScale.y, i);
            float z = options.pivot.localScale.z * Mathf.Pow(options.offset.localScale.z, i);
            ds.transform.localScale = Vector3.Lerp(options.pivot.localScale, options.offset.localScale, t01);
        }


        public static void Offset_Last_plus(DepthSlicerOptions options, SpriteRenderer ds, EzSpriteAnimator animator, int i, int _count) {
            float t01 = i / (float) _count;

            if(i == 0) {
                ds.transform.localPosition = options.pivot.localPosition;
                ds.transform.localEulerAngles = options.pivot.localEulerAngles;
                ds.transform.localScale = options.pivot.localScale;
            } else {
                ds.transform.localPosition = animator.depthSlicerSprites[i - 1].transform.localPosition + animator.depthSlicerSprites[i - 1].transform.TransformVector(options.offset.localPosition);
                ds.transform.localEulerAngles = animator.depthSlicerSprites[i - 1].transform.localEulerAngles + options.offset.localEulerAngles; //animator.depthSlicerSprites[i-1].transform.TransformVector();

                ds.transform.localScale = Vector3.Lerp(options.pivot.localScale, options.offset.localScale, t01);
            }

            /*
             * 
            ds.transform.localPosition    = options.pivot.localPosition    + options.offset.localPosition    * i;
            ds.transform.localEulerAngles = options.pivot.localEulerAngles;// + offset.localEulerAngles * i;
            if(options.offset.localEulerAngles.x!=0){
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition),new Vector3(1, 0,0),options.offset.localEulerAngles.x*i);
            }
            if(options.offset.localEulerAngles.y!=0){
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition),new Vector3(0,1,0),options.offset.localEulerAngles.y*i);
            }
            if(options.offset.localEulerAngles.z != 0) {
                ds.transform.RotateAround(ds.transform.TransformPoint(-options.pivot.localPosition),new Vector3(0,0,1),options.offset.localEulerAngles.z*i);
            }
            var x= options.pivot.localScale.x* Mathf.Pow(options.offset.localScale.x, i);
            var y=options.pivot.localScale.y*  Mathf.Pow(options.offset.localScale.y, i);
            var z= options.pivot.localScale.z* Mathf.Pow(options.offset.localScale.z, i);
            ds.transform.localScale = Vector3.Lerp(options.pivot.localScale, options.offset.localScale, t01);
             */
        }
    }
}


//////////////
public enum EasyAnimatorSpriteActions {
    SHOWFRAME,
    PAUSE,
    QUEUENEXT
}

public enum LoopMode {
    LOOP = 0,
    PINGPONG = 1,
    PLAYONCE_THEN_PAUSE = 2,
    PLAYONCE_THEN_QUEUE_NEXT = 3
}


[ExecuteInEditMode]
public
    abstract class EzAnimator<T, Y> : EZAnimatorBase where Y : EZAnimatorStateBase<T> {
    public void SetAnimationState(string newState) {
        stateName = newState;
    }

    public override void SetAnimationStateIndex(int newStateIndex) {
        if(newStateIndex >= 0 && newStateIndex < states.Count) {
            stateName = states[newStateIndex].name;
        }
    }

    public List<Y> states = new List<Y>();

    [System.NonSerialized]
    public Y currentState;

    protected virtual void ApplyFrame(T frame, Y s) {
    }

    public override float durationFrames {
        get {
            if(currentState == null) {
                return 0;
            }

            return currentState.durationFrames(loopMode);
        }
    }

    public override int states_Count {
        get { return states.Count; }
    }

    public override string state_name(int index) {
        return states[index].name;
    }

    public override bool IsStateValid(string stateName) {
        if(currentState.name == stateName) {
            return true;
        }

        for(int i = 0; i < states.Count; i++) { // i think this is faster than using a dictionary for small sized arrays.
            if(states[i].name == stateName) {
                return true;
            }
        }

        return false;
    }

    public override string onQueueStateName {
        get {
            if(currentState == null) {
                return "";
            } else {
                return currentState.onQueueStateName;
            }
        }
    }


    private void UpdateState() {
        if(states.Count > 0) {
            if(currentState == null || currentState.name != stateName) {
                int _currentStateIndex = -1;
                bool fail = true;
                for(int i = 0; i < states.Count; i++) {
                    if(currentState != null && states[i].name == currentState.name) {
                        _currentStateIndex = i;
                    }

                    if(states[i].name == stateName) {
                        _currentStateIndex = i;
                        currentState = states[i];
                        fail = false;
                        break;
                    }
                }

                if(fail) {
                    currentState = states[(_currentStateIndex + 1) % states.Count];
                }

                playing = true;
                currentTimeFrames = 0;
                loopMode = currentState.loopMode;
                stateName = currentState.name;
                needsFrameUpdate = true;
                onStateUpdate(stateName);
            }
        }
    }

    private bool needsFrameUpdate = false;
    private int oldFrameIndex = 0;
    private bool wasPausedAtEndOfAnimation = false;


    #region REPAIR_BADNAMES

#if UNITY_EDITOR


    private string CleanRegex(string input) {
        input = input.ToLower();
        input = input.Replace(" ", "_");
        input = input.Replace("-", "_");
        input = input.Replace("#", "_");
        input = input.Replace("@", "_");
        input = input.Replace(":", "_");
        return System.Text.RegularExpressions.Regex.Replace(input, "[^a-z0-9_]+", "");
    }


    private bool IsClean(string input) {
        return System.Text.RegularExpressions.Regex.IsMatch(input, @"^[a-z0-9_]+$");
    }

    private string TryNameState(int stateIndex, string goalState) {
        if(string.IsNullOrWhiteSpace(goalState)) {
            string name = states[stateIndex].GetSpriteOrTextureName(0);
            Debug.Log("NAME HERE " + name);
            if(name != null) {
                name = System.Text.RegularExpressions.Regex.Replace(name, @"\d+$", "");
                name = CleanRegex(name);
                name = name.Trim('_');
            }

            if(string.IsNullOrWhiteSpace(name)) {
                goalState = "state_" + stateIndex;
            } else {
                goalState = name;
            }
        }

        goalState = CleanRegex(goalState);

        for(int i = 0; i < states.Count; i++) {
            if(i != stateIndex && states[i].name == goalState) {
                Debug.Log("WARNING State is duplicated We will rename something");
                return goalState + "alt";
            }
        }

        return goalState;
    }

    // [Header("Warning: changes will not save while in edit mode.")]
    //   public bool showMenu = false;
    //    private EZAnimatorMenu ezMenu;

    public override void Repair() {
        currentTimeFrames = 0;
        for(int i = 0; i < states.Count; i++) {
            Y state = states[i];
            state.name = TryNameState(i, state.name);
            state.onQueueStateName = CleanRegex(state.onQueueStateName);
        }

        UpdateState();
    }

    private HashSet<string> ___stateNamesEditorOnlyForRepair = new HashSet<string>();

    public override bool needsRepair_SLOW {
        get {
            ___stateNamesEditorOnlyForRepair.Clear();
            for(int i = 0; i < states.Count; i++) {
                if(string.IsNullOrWhiteSpace(states[i].name)) {
                    return true;
                }

                if(___stateNamesEditorOnlyForRepair.Contains(states[i].name)) {
                    return true;
                }

                if(!IsClean(states[i].name)) {
                    Debug.Log(states[i].name + " not clean");
                    return true;
                }

                if(states[i].onQueueStateName != "" && states[i].onQueueStateName != null) {
                    if(!IsClean(states[i].onQueueStateName)) {
                        Debug.Log(states[i].onQueueStateName + " not clean");
                        return true;
                    }
                }

                ___stateNamesEditorOnlyForRepair.Add(states[i].name);
            }

            return false;
        }
    }

    public override string nextStateError {
        get {
            string errorMessage = "";
            for(int i = 0; i < states.Count; i++) {
                if(states[i].onQueueStateName == "" || states[i].onQueueStateName == null) {
                    continue;
                }

                if(___stateNamesEditorOnlyForRepair.Contains(states[i].onQueueStateName)) {
                    continue;
                }

                errorMessage += states[i].name + " next state is invalid: " + states[i].onQueueStateName + System.Environment.NewLine;
            }

            return errorMessage;
        }
    }

#endif

    #endregion

    public virtual void Update() {
        float previousFrame = currentTimeFrames;
        if(playing) {
            if(wasPausedAtEndOfAnimation) {
                wasPausedAtEndOfAnimation = false;
                currentTimeFrames = 0;
            } else {
                currentTimeFrames += Time.deltaTime * options.framesPerSecond * options.playbackRate;
            }
        }

        totalTimeSecondsFromAwake += Time.deltaTime;


        UpdateState();
        if(currentState != null) {
            switch(currentState.GetState(Mathf.FloorToInt(currentTimeFrames), loopMode)) {
                case EasyAnimatorSpriteActions.SHOWFRAME:
                    break;
                case EasyAnimatorSpriteActions.PAUSE:
                    // frameIndexFloat = 0;
                    playing = false;
                    wasPausedAtEndOfAnimation = true;
                    break;
                case EasyAnimatorSpriteActions.QUEUENEXT:
                    currentTimeFrames = 0;
                    stateName = currentState.onQueueStateName;
                    UpdateState();
                    break;
                //   case EasyAnimatorSpriteActions.STOP:
                //      vcrState = VCRState.STOP;
                //      break;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }
        }

        if(currentState != null) {
            // we will call frame update even if there is one frame int the animation...
            if(Mathf.FloorToInt(previousFrame) != Mathf.FloorToInt(currentTimeFrames)) {
                needsFrameUpdate = true;
            }


            if(playing) {
                currentTimeFrames = Mathf.Repeat(currentTimeFrames, currentState.durationFrames(loopMode));
            } else {
                currentTimeFrames = Mathf.Clamp(currentTimeFrames, 0, currentState.durationFrames(loopMode) - .0001f);
            }

            int newFrameIndex = Mathf.FloorToInt(currentTimeFrames);


            if(playing) {
                deltaTime += Time.deltaTime;
            }

            oldFrameIndex = newFrameIndex;
            if(needsFrameUpdate) {
                needsFrameUpdate = false;
                onFrameUpdate(deltaTime);
                deltaTime = 0;

                T newFrame = currentState.GetFrame(newFrameIndex, loopMode);
                ApplyFrame(newFrame, currentState);
            }
        }
    }
}


[System.Serializable]
public class EzAnimatorStateSprite : EZAnimatorStateBase<Sprite> {
    [System.Serializable]
    public class SpriteOptions {
        public bool flipX = false;
        public bool flipY = false;
    }

    public SpriteOptions spriteOptions = new SpriteOptions();


    public override string GetSpriteOrTextureName(int index) {
        Sprite f = GetFrame(index);
        if(f != null) {
            return f.name;
        }

        return null;
    }
}


public abstract class EZAnimatorBase : MonoBehaviour {
    public string _stateName;

    public string stateName {
        get { return _stateName; }
        set {
            if(IsStateValid(value)) {
                _stateName = value;
            } else {
                Debug.LogWarning("State name " + value + " not valid on " + gameObject.name);
            }
        }
    }

    protected float deltaTime = 0;

    protected float _currentTimeFrames = 0;

    [System.NonSerialized]
    public float totalTimeSecondsFromAwake = 0;

    public float playbackRate {
        get { return options.playbackRate; }
        set { options.playbackRate = value; }
    }

    public float framesPerSecond {
        get { return options.framesPerSecond; }
        set { options.framesPerSecond = value; }
    }


    public void OnEnable() {
        deltaTime = 0;
        totalTimeSecondsFromAwake = 0;
    }

    public System.Action<float> onFrameUpdate = deltaTime => { };
    public System.Action<string> onStateUpdate = stateName => { };

    [System.NonSerialized]
    public LoopMode loopMode = LoopMode.LOOP;

    public float currentTimeFrames {
        get { return _currentTimeFrames; }
        set { _currentTimeFrames = value; }
    }

    public bool _playing = true;

    public bool playing {
        get { return _playing; }
        set { _playing = value; }
    }

    /// BASIC VCR STUFF
    public void Play() {
        playing = true;
    }

    public void Pause() {
        playing = false;
    }

    public void Rewind() {
        currentTimeFrames = 0;
    }


    public int stateIndexSlow { // avoid using this function 
        get {
            if(states_Count >= 1) {
                for(int i = 0; i < states_Count; i++) {
                    if(state_name(i) == stateName) {
                        return i;
                    }
                }
            }

            return 0;
        }
    }

    public abstract void SetAnimationStateIndex(int newStateIndex);

    public void NextStateByIndex() {
        SetAnimationStateIndex((stateIndexSlow + 1) % states_Count);
    }

    public abstract float durationFrames { get; }

    public float currentTime01 {
        get {
            if(durationFrames <= 0.0f) {
                return 0;
            }

            return currentTimeFrames / durationFrames;
        }
        set { currentTimeFrames = value * durationFrames; }
    }

    public float currentTimeSeconds {
        get {
            if(options.framesPerSecond * options.playbackRate == 0) {
                return 0;
            }

            return currentTimeFrames / options.framesPerSecond;
        }
        set { currentTimeFrames = value * options.framesPerSecond; }
    }

    public float durationSeconds {
        get {
            if(options.framesPerSecond == 0) {
                return 0;
            }

            return durationFrames / options.framesPerSecond;
        }
    }


    [System.Serializable]
    public class Options {
        public float playbackRate = 1;
        public float framesPerSecond = 12;
    }

    public Options options = new Options();

    public abstract int states_Count { get; }
    public abstract string state_name(int index);

    public abstract bool IsStateValid(string stateName);

    public abstract string onQueueStateName { get; }


#if UNITY_EDITOR

    public abstract void Repair();
    public abstract bool needsRepair_SLOW { get; }
    public abstract string nextStateError { get; }
#endif
}


public abstract class EZAnimatorMonoBehavior : MonoBehaviour {
    protected abstract void OnFrameUpdate(float deltaTime);
    protected abstract void OnStateUpdate(string newStateName);


    [Header("However, you can connect it manually if you want to have events triggered by a different animator.")]
    [Header("This will get connected automatically if you attach this to a game object with the matching type.")]
    public EZAnimatorBase ezAnimator;

    public virtual void OnEnable() {
        if(ezAnimator == null) {
            ezAnimator = GetComponent<EZAnimatorBase>();
        }

        if(ezAnimator) {
            ezAnimator.onFrameUpdate += OnFrameUpdate;
            ezAnimator.onStateUpdate += OnStateUpdate;
        }
    }

    public virtual void OnDisable() {
        try {
            if(ezAnimator) {
                ezAnimator.onFrameUpdate -= OnFrameUpdate;
                ezAnimator.onStateUpdate -= OnStateUpdate;
            }
        } catch {
        }
    }
}

[System.Serializable]
public abstract class EZAnimatorStateBase<T> {
    public string name = null;


    public bool isQueueable {
        get {
            switch(loopMode) {
                case LoopMode.LOOP:
                    return false;
                case LoopMode.PINGPONG:
                    return false;
                case LoopMode.PLAYONCE_THEN_PAUSE:
                    return false;
                case LoopMode.PLAYONCE_THEN_QUEUE_NEXT:
                    return true;
                default:
                    throw new System.ArgumentOutOfRangeException();
            }
        }
    }

    [ConditionalShowAttribute(nameof(isQueueable))]
    public string onQueueStateName = "";

    public List<T> _frames = new List<T>();

    public LoopMode loopMode {
        get { return options.loopMode; }
        set { options.loopMode = value; }
    }

    public abstract string GetSpriteOrTextureName(int index);


    public EZAnimatorStateOptions options = new EZAnimatorStateOptions();

    public bool inRange(int index, LoopMode? _onFinish = null) {
        if(index < 0) {
            return false;
        }

        return index < durationFrames(_onFinish);
    }

    private T lastFrame;

    public T GetFrame(int index, LoopMode? _onFinish = null) {
        if(_frames.Count > 0) {
            switch(_onFinish.GetValueOrDefault(loopMode)) {
                case LoopMode.LOOP:
                    lastFrame = _frames[negmod(index, _frames.Count)];
                    break;

                case LoopMode.PINGPONG:
                    int frameCount__2n_minus_2 = durationFrames(_onFinish);
                    int pingpongindex_0_to_2n_minus_2 = negmod(index, frameCount__2n_minus_2);
                    if(pingpongindex_0_to_2n_minus_2 < _frames.Count) {
                        lastFrame = _frames[pingpongindex_0_to_2n_minus_2];
                    } else {
                        lastFrame = _frames[frameCount__2n_minus_2 - pingpongindex_0_to_2n_minus_2];
                    }

                    break;

                case LoopMode.PLAYONCE_THEN_PAUSE:
                    lastFrame = _frames[Mathf.Clamp(index, 0, _frames.Count - 1)];
                    break;

                case LoopMode.PLAYONCE_THEN_QUEUE_NEXT:
                    lastFrame = _frames[Mathf.Clamp(index, 0, _frames.Count - 1)];
                    break;

                default:
                    break;
            }
        }

        return lastFrame;
    }

    public int durationFrames(LoopMode? _onFinish = null) {
        switch(_onFinish.GetValueOrDefault(loopMode)) {
            case LoopMode.LOOP:
                return _frames.Count;
            case LoopMode.PINGPONG:
                if(_frames.Count < 3) {
                    return _frames.Count;
                }

                return _frames.Count * 2 - 2;
            case LoopMode.PLAYONCE_THEN_PAUSE:
                return _frames.Count;
            case LoopMode.PLAYONCE_THEN_QUEUE_NEXT:
                return _frames.Count;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
    }

    public EasyAnimatorSpriteActions GetState(int index, LoopMode? _onFinish = null) {
        if(inRange(index)) {
            return EasyAnimatorSpriteActions.SHOWFRAME;
        }

        switch(_onFinish.GetValueOrDefault(loopMode)) {
            case LoopMode.LOOP:
                return EasyAnimatorSpriteActions.SHOWFRAME;
            case LoopMode.PINGPONG:
                return EasyAnimatorSpriteActions.SHOWFRAME;
            case LoopMode.PLAYONCE_THEN_PAUSE:
                return EasyAnimatorSpriteActions.PAUSE;
            case LoopMode.PLAYONCE_THEN_QUEUE_NEXT:
                return EasyAnimatorSpriteActions.QUEUENEXT;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
    }

    private int negmod(int x, int m) {
        return (x % m + m) % m;
    }
}


[System.Serializable]
public class EZAnimatorStateOptions {
    public LoopMode loopMode = LoopMode.LOOP;
}

public class TextureEZAnimator : EzAnimator<Texture2D, EzTextureAnimator> {
    /*  private void Awake() {
          if(advancedOptions.automaticallyConnectMaterial || advancedOptions.material) {
              MeshRenderer mr = GetComponent<MeshRenderer>();
              if(mr) {
                  if(advancedOptions.automaticallyConnectMaterial) {
                      sharedMaterialtemp = mr.sharedMaterial;
                      mr.sharedMaterial = new Material(sharedMaterialtemp);
                  }
              }
          }
      }
  
      private Material sharedMaterialtemp;
      private void OnDestroy() {
          if( advancedOptions.automaticallyConnectMaterial) {
              MeshRenderer mr = GetComponent<MeshRenderer>();
              if(mr && mr.sharedMaterial == null) {
                  mr.sharedMaterial = sharedMaterialtemp;
              }
          }
      }
  */
    [System.Serializable]
    public class AdvancedOptions {
        [UnityEngine.Serialization.FormerlySerializedAs("targetSharedMaterial")]
        public Material material;

        [Header("some materials use a different texture name")]
        public string materialTextureName = "_MainTex";
    }

    [Header("WARNING : If the material's parameters at the bottom of the inspector", order = 0)]
    [Space(-10, order = 1)]
    [Header("are grayed out you need to create a new material.", order = 2)]
    [Space(-10, order = 3)]
    [Header("Then assign it to the object.", order = 4)]
    [Space(10, order = 5)]
    public AdvancedOptions advancedOptions = new AdvancedOptions();


    protected override void ApplyFrame(Texture2D frame, EzTextureAnimator state) {
        base.ApplyFrame(frame, state);
        if(advancedOptions.material != null) {
            if(advancedOptions.materialTextureName == "_MainTex") {
                advancedOptions.material.mainTexture = frame;
            } else {
                advancedOptions.material.SetTexture(advancedOptions.materialTextureName, frame);
            }
        }
    }
}


#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(EzSpriteAnimator))]
public class SpriteEZAnimatorEditor : EZAnimatorBaseEditor {
    public override void OnSceneGUI() {
        base.OnSceneGUI();
        EzSpriteAnimator animatorBase = target as EzSpriteAnimator;
        animatorBase.spriteOptions.depthSlicerOptions.positionDirty = true;
    }
}

[UnityEditor.CustomEditor(typeof(TextureEZAnimator))]
public class TextureEZAnimatorEditor : EZAnimatorBaseEditor {
}

public abstract class EZAnimatorBaseEditor : UnityEditor.Editor {
    public bool HelperMode(string lhs, string s, string rhs, bool shows, float horizontal, float pad, float lrs, float magic) {
        bool val = false;
        UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));

        if(shows) {
            GUILayout.Label(s, GUILayout.Width(pad));
        } else {
            GUILayout.Label("  ", GUILayout.Width(pad));
        }

        if(GUILayout.Button(rhs, GUILayout.Width(horizontal - lrs - pad - magic))) {
            val = true;
        }

        UnityEditor.EditorGUILayout.EndHorizontal();
        return val;
    }

    private static bool showGUI = true;


    public virtual void OnSceneGUI() {
        /*
                var c = Event.current.displayIndex;
                if(Event.current.type == EventType.Layout) {
                    Debug.Log("time"+Time.frameCount);
                    
                }
                GUILayout.Label(Event.current.type.ToString());
                GUILayout.Label(Event.current.displayIndex.ToString());
                GUILayout.Label(Time.time.ToString());
                */

        EZAnimatorBase ezAnimatorBase = target as EZAnimatorBase;
        UnityEditor.Handles.BeginGUI();
        float horizontal = 300;
        float lrs = 70;
        float pad = 20;
        float magic = 8;


        showGUI = GUILayout.Toggle(showGUI, "EzAnimator");

        if(showGUI) {
            UnityEditor.EditorGUILayout.BeginVertical();
            {
                ////// VCR 
                UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                GUILayout.Label("VCR", GUILayout.Width(lrs));
                GUILayout.Label("  ", GUILayout.Width(20));
                if(GUILayout.Button("<<")) {
                    ezAnimatorBase.currentTimeFrames -= 1;
                }

                if(GUILayout.Button("|>")) {
                    ezAnimatorBase.playing = !ezAnimatorBase.playing;
                }

                if(GUILayout.Button(">>")) {
                    ezAnimatorBase.currentTimeFrames += 1;
                }

                UnityEditor.EditorGUILayout.EndHorizontal();
            }
            {
                ////// States 
                UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                GUILayout.Label("STATE", GUILayout.Width(lrs));
                UnityEditor.EditorGUILayout.BeginVertical();

                if(ezAnimatorBase.states_Count == 0) {
                    UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                    GUILayout.Label("->", GUILayout.Width(pad));
                    GUILayout.Label("Please add at least one state!", GUILayout.Width(horizontal - lrs - pad - magic));
                    UnityEditor.EditorGUILayout.EndHorizontal();
                }

                for(int i = 0; i < ezAnimatorBase.states_Count; i++) {
                    UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                    if(ezAnimatorBase.stateName == ezAnimatorBase.state_name(i)) {
                        GUILayout.Label("->", GUILayout.Width(pad));
                    } else {
                        GUILayout.Label("  ", GUILayout.Width(pad));
                    }

                    if(GUILayout.Button(ezAnimatorBase.state_name(i), GUILayout.Width(horizontal - lrs - pad - magic))) {
                        ezAnimatorBase.stateName = ezAnimatorBase.state_name(i);
                    }

                    UnityEditor.EditorGUILayout.EndHorizontal();
                }

                UnityEditor.EditorGUILayout.EndVertical();
                UnityEditor.EditorGUILayout.EndHorizontal();
            }
            {
                ////// TIMELINE 
                UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                GUILayout.Label("Time", GUILayout.Width(lrs));
                GUILayout.Label("  ", GUILayout.Width(pad));

                if(ezAnimatorBase.durationFrames > 0) {
                    float b4 = ezAnimatorBase.currentTime01;
                    ezAnimatorBase.currentTime01 = GUILayout.HorizontalSlider(ezAnimatorBase.currentTime01, 0, 1);

                    if(ezAnimatorBase.currentTime01 != b4) {
                        ezAnimatorBase.Pause();
                    }
                } else {
                    GUILayout.Label("Please add at least one frame to this state");
                }

                UnityEditor.EditorGUILayout.EndHorizontal();
            }

            {
                ////// Loop Mode 
                UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                GUILayout.Label("Loop Mode", GUILayout.Width(lrs));
                UnityEditor.EditorGUILayout.BeginVertical();

                if(HelperMode("", " *", "->-> (Loop)", ezAnimatorBase.loopMode == LoopMode.LOOP, horizontal, pad, lrs, magic)) {
                    ezAnimatorBase.loopMode = LoopMode.LOOP;
                    ezAnimatorBase.playing = true;
                }

                if(HelperMode("", " *", "<--> (Ping Pong)", ezAnimatorBase.loopMode == LoopMode.PINGPONG, horizontal, pad, lrs, magic)) {
                    ezAnimatorBase.loopMode = LoopMode.PINGPONG;
                    ezAnimatorBase.playing = true;
                }

                if(HelperMode("", " *", "->|| (Play then Pause)", ezAnimatorBase.loopMode == LoopMode.PLAYONCE_THEN_PAUSE, horizontal, pad, lrs, magic)) {
                    ezAnimatorBase.loopMode = LoopMode.PLAYONCE_THEN_PAUSE;
                    ezAnimatorBase.playing = true;
                }

                if(HelperMode("", " *", "->Q (Play then Queue)", ezAnimatorBase.loopMode == LoopMode.PLAYONCE_THEN_QUEUE_NEXT, horizontal, pad, lrs, magic)) {
                    ezAnimatorBase.loopMode = LoopMode.PLAYONCE_THEN_QUEUE_NEXT;
                    ezAnimatorBase.playing = true;
                }

                UnityEditor.EditorGUILayout.EndVertical();
                UnityEditor.EditorGUILayout.EndHorizontal();
            }

            { // Next state

                UnityEditor.EditorGUILayout.BeginHorizontal(GUILayout.Width(horizontal));
                GUILayout.Label("Next State", GUILayout.Width(lrs));
                UnityEditor.EditorGUILayout.BeginVertical();

                if(HelperMode("", "->Q", string.IsNullOrEmpty(ezAnimatorBase.onQueueStateName) ? "NEXT" : ezAnimatorBase.onQueueStateName, ezAnimatorBase.loopMode == LoopMode.PLAYONCE_THEN_QUEUE_NEXT, horizontal, pad, lrs, magic)) {
                    ezAnimatorBase.stateName = ezAnimatorBase.onQueueStateName;
                }

                UnityEditor.EditorGUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(pad));
                if(ezAnimatorBase.loopMode != LoopMode.PLAYONCE_THEN_QUEUE_NEXT) {
                    GUILayout.Label("(only used in ->Q mode)");
                }

                UnityEditor.EditorGUILayout.EndHorizontal();
                UnityEditor.EditorGUILayout.EndVertical();


                UnityEditor.EditorGUILayout.EndHorizontal();
            }

            {
                if(ezAnimatorBase.needsRepair_SLOW) {
                    if(GUILayout.Button("Repair States")) {
                        ezAnimatorBase.Repair();
                    }

                    GUILayout.Label("State Names must be lowercase_numbers_or_underscores");
                }

                if(ezAnimatorBase.nextStateError != "") {
                    GUILayout.Label("NextState must be a valid state name or empty.");
                    GUILayout.Label("We can not fix this automatically but see the issue below.");
                    GUILayout.Label(ezAnimatorBase.nextStateError);
                }
            }
            UnityEditor.EditorGUILayout.EndVertical();
        }

        UnityEditor.Handles.EndGUI();
    }
}

#endif


public static class EasyVectorExtensions {
    public static Vector3 SetX(this Vector3 vec, float x) {
        return new Vector3(x, vec.y, vec.z);
    }

    public static Vector3 SetY(this Vector3 vec, float y) {
        return new Vector3(vec.x, y, vec.z);
    }

    public static Vector3 SetZ(this Vector3 vec, float z) {
        return new Vector3(vec.x, vec.y, z);
    }

    public static Vector3 ScaleX(this Vector3 vec, float x) {
        return new Vector3(vec.x * x, vec.y, vec.z);
    }

    public static Vector3 ScaleY(this Vector3 vec, float y) {
        return new Vector3(vec.x, vec.y * y, vec.z);
    }

    public static Vector3 ScaleZ(this Vector3 vec, float z) {
        return new Vector3(vec.x, vec.y, vec.z * z);
    }

    public static Vector3 AddX(this Vector3 vec, float x) {
        return new Vector3(vec.x + x, vec.y, vec.z);
    }

    public static Vector3 AddY(this Vector3 vec, float y) {
        return new Vector3(vec.x, vec.y + y, vec.z);
    }

    public static Vector3 AddZ(this Vector3 vec, float z) {
        return new Vector3(vec.x, vec.y, vec.z + z);
    }
}

public class EzEventBase : MonoBehaviour {
    protected EZAnimatorBase target;

    protected virtual void Awake() {
        target = gameObject.GetComponent<EZAnimatorBase>();
    }

    public void ApplyEvent(EzEventHandler mEventHandler) {
        mEventHandler.ApplyEvent(target, gameObject);
        SetNewState(mEventHandler.setStateTo);
    }

    public virtual void SetNewState(string newStateName) {
    }


    [System.Serializable]
    public class EzEventHandler {
        public string setStateTo = "";
        public bool debug = false;
        public string debugMessage = "Event Happened!";
        public UnityEngine.Events.UnityEvent action;

        public void ApplyEvent(EZAnimatorBase target, GameObject go) {
            if(debug) {
                string message = debugMessage + " happened on " + go.name;
                if(target) {
                    if(!string.IsNullOrWhiteSpace(setStateTo)) {
                        message = message + " state:" + target.stateName + " -> " + setStateTo;
                    }
                } else {
                    message = message + ". No animator attached.";
                }

                Debug.Log(message);
            }

            if(!string.IsNullOrWhiteSpace(setStateTo) && target) {
                target.stateName = setStateTo;
            }

            action.Invoke();
        }
    }
}

public class EzEventPicky : EzEventBase {
    public string requireOtherGameObjectToHaveTag = "";
    public string requireOtherGameObjectToHaveName = "";

    public void TryApplyEvent(EzEventHandler ezEventHandler, GameObject go) {
        bool matched = false;
        if(string.IsNullOrWhiteSpace(requireOtherGameObjectToHaveName) && string.IsNullOrWhiteSpace(requireOtherGameObjectToHaveTag)) {
            matched = true;
        } else if(go.name == requireOtherGameObjectToHaveName) {
            matched = true;
        } else if(go.tag == requireOtherGameObjectToHaveTag) {
            matched = true;
        }

        if(matched) {
            ApplyEvent(ezEventHandler);
        }
    }
}
/*

[UnityEditor.CustomPropertyDrawer(typeof(EzSpriteAnimator.DepthSlicerPresets))]
public class DepthSlicerPresetsEditor : UnityEditor.PropertyDrawer {
    public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label) {
        UnityEditor.EditorGUI.BeginProperty(position, label, property);
        UnityEditor.EditorGUI.PropertyField(position, property, label, true);
        UnityEditor.EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(UnityEditor.SerializedProperty property, GUIContent label) {
//        EzSpriteAnimator.DepthSlicerPresets preset =  property.serializedObject.targetObject as EzSpriteAnimator.DepthSlicerPresets;
        return UnityEditor.EditorGUIUtility.singleLineHeight;
    }
}*/


/*

// This defines how the ColorSpacer should be drawn
// in the inspector, when inspecting a GameObject with
// a MonoBehaviour which uses the ColorSpacer attribute

[UnityEditor.CustomPropertyDrawer(typeof(EzSpriteAnimator.DepthSlicerPresets2.DepthSlicerOptionsPreset))]
public class DepthSlicerOptionsPresetDrawer :UnityEditor. PropertyDrawer
{
    const int  buttonHeight = 20;
    private const int buttonYPadding = 5;
    public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
    {
        UnityEditor.        EditorGUI.PropertyField(position, property, label, true);
        if (property.isExpanded)
        {
            
            EzSpriteAnimator.DepthSlicerPresets2.DepthSlicerOptionsPreset source  = property.GetParent<EzSpriteAnimator.DepthSlicerPresets2.DepthSlicerOptionsPreset>(0);
            EzSpriteAnimator parent = property.GetParent<EzSpriteAnimator>(2);
            EzSpriteAnimator.DepthSlicerOptions targetOptions = null;
            if(parent != null) {
                targetOptions = parent.spriteOptions.depthSlicerOptions;
            } else {
                targetOptions = property.GetParent<EzDepthSlicerControlBehavior.DepthSlicerOptionsControl>(1);
            }
            
            
            
            if (GUI.Button(new Rect(position.xMin + 30f, position.yMax - buttonHeight, position.width - 30f, buttonHeight), "Apply Preset"))
            {
                if(targetOptions != null) {
                    targetOptions.ApplyOptions(source, false);
                }
            }

            if (GUI.Button(new Rect(position.xMin + 30f, position.yMax - buttonHeight*2-buttonYPadding, position.width - 30f, buttonHeight), "Copy Preset"))
            {
                if(targetOptions != null) {
                    source      .ApplyOptions(targetOptions, false);
                }
            }
        }
    }
 
    public override float GetPropertyHeight(UnityEditor.SerializedProperty property, GUIContent label)
    {
        if (property.isExpanded)
            return UnityEditor.EditorGUI.GetPropertyHeight(property) + buttonHeight*2 + buttonYPadding;
        return UnityEditor.EditorGUI.GetPropertyHeight(property);
    }
}*/

/*
[UnityEditor.CustomPropertyDrawer(typeof(EzDepthSlicerControlBehavior.DepthSlicerOptionsControl))]
public class DepthSlicerControlOptionsDrawer : DepthSlicerOptionsDrawer {
}*/
