﻿using System;
using UnityEngine;

//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)
//Modified by: -  @drclementshimizu

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property |
                AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
public class ConditionalHideAttribute : PropertyAttribute {
    public string ConditionalSourceField = "";
    public bool HideInInspector = true;


    public virtual bool invertResult {
        get { return true; }
    }

    /* Enum */
    public int enumIndex;

    /**
    * hideInInspector - true = hide, false = deactivate
    *
    */

    // Use this for initialization
    public ConditionalHideAttribute(string conditionalSourceField) {
        ConditionalSourceField = conditionalSourceField;
        HideInInspector = true;
    }

    public ConditionalHideAttribute(string conditionalSourceField, bool hideInInspector) {
        ConditionalSourceField = conditionalSourceField;
        HideInInspector = hideInInspector;
    }


    public ConditionalHideAttribute(bool hideInInspector = true) {
        ConditionalSourceField = "";
        HideInInspector = hideInInspector;
    }

    /* Enum */
    public ConditionalHideAttribute(string conditionalSourceField, int value) {
        ConditionalSourceField = conditionalSourceField;
        enumIndex = value;
        HideInInspector = true;
    }
}


public class ConditionalShowAttribute : ConditionalHideAttribute {
    public override bool invertResult {
        get { return false; }
    }

    public ConditionalShowAttribute(bool hideInInspector = true) : base(hideInInspector) {
    }

    public ConditionalShowAttribute(string conditionalSourceField, int value) : base(conditionalSourceField, value) {
    }

    public ConditionalShowAttribute(string conditionalSourceField) : base(conditionalSourceField) {
    }

    public ConditionalShowAttribute(string conditionalSourceField, bool hideInInspector) : base(conditionalSourceField, hideInInspector) {
    }
}
