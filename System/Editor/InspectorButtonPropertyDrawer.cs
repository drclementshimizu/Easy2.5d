using UnityEngine;

[UnityEditor.CustomPropertyDrawer(typeof(InspectorButtonAttribute))]
public class InspectorButtonPropertyDrawer : UnityEditor.PropertyDrawer {
    private System.Reflection.MethodInfo _eventMethodInfo = null;

    public override void OnGUI(Rect position, UnityEditor.SerializedProperty prop, GUIContent label) {
        InspectorButtonAttribute inspectorButtonAttribute = (InspectorButtonAttribute) attribute;
        Rect buttonRect = new Rect(position.x + (position.width - inspectorButtonAttribute.ButtonWidth) * 0.5f, position.y, inspectorButtonAttribute.ButtonWidth, position.height);
        if(GUI.Button(buttonRect, label.text)) {
            System.Type eventOwnerType = prop.serializedObject.targetObject.GetType();
            string eventName = inspectorButtonAttribute.MethodName;

            if(_eventMethodInfo == null) {
                _eventMethodInfo = eventOwnerType.GetMethod(eventName, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic);
            }

            if(_eventMethodInfo != null) {
                _eventMethodInfo.Invoke(prop.serializedObject.targetObject, null);
            } else {
                Debug.LogWarning(string.Format("InspectorButton: Unable to find method {0} in {1}", eventName, eventOwnerType));
            }
        }
    }
}
