﻿using UnityEngine;

//Original version of the ConditionalHideAttribute created by Brecht Lecluyse (www.brechtos.com)
//Modified by: - @drclementshimizu

[UnityEditor.CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
public class ConditionalHidePropertyDrawer : ConditionalHidePropertyDrawerBASE<ConditionalHideAttribute> {
}

[UnityEditor.CustomPropertyDrawer(typeof(ConditionalShowAttribute))]
public class ConditionalShowPropertyDrawer : ConditionalHidePropertyDrawerBASE<ConditionalShowAttribute> {
}

public class ConditionalHidePropertyDrawerBASE<T> : UnityEditor.PropertyDrawer where T : ConditionalHideAttribute {
    public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label) {
        T condHAtt = (T) attribute;

        bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

        bool wasEnabled = GUI.enabled;
        GUI.enabled = enabled;

        if(!condHAtt.HideInInspector || enabled) {
            UnityEditor.EditorGUI.PropertyField(position, property, label, true);
        }

        GUI.enabled = wasEnabled;
    }

    public override float GetPropertyHeight(UnityEditor.SerializedProperty property, GUIContent label) {
        T condHAtt = (T) attribute;
        bool enabled = GetConditionalHideAttributeResult(condHAtt, property);

        if(!condHAtt.HideInInspector || enabled) {
            return UnityEditor.EditorGUI.GetPropertyHeight(property, label);
        } else {
            //The property is not being drawn
            //We want to undo the spacing added before and after the property
            return UnityEditor.EditorGUIUtility.standardVerticalSpacing;
            //return 0.0f;
        }


        /*
        //Get the base height when not expanded
        var height = base.GetPropertyHeight(property, label);
        // if the property is expanded go thru all its children and get their height
        if (property.isExpanded)
        {
            var propEnum = property.GetEnumerator();
            while (propEnum.MoveNext())
                height += EditorGUI.GetPropertyHeight((SerializedProperty)propEnum.Current, GUIContent.none, true);
        }
        return height;*/
    }


    public static bool GetConditionalHideAttributeResult(T condHAtt, UnityEditor.SerializedProperty property) {
        bool enabled = false;

        //Get the full relative property path of the sourcefield so we can have nested hiding
        string propertyPath = property.propertyPath; //returns the property path of the property we want to apply the attribute to
        string conditionPath = propertyPath.Replace(property.name, condHAtt.ConditionalSourceField); //changes the path to the conditionalsource property path
        Object targetObject = property.serializedObject.targetObject;
        object result = RecursiveGetProperty(targetObject, conditionPath);

        enabled = CheckPropertyType(result);

        //        Debug.LogWarning(enabled);
        //    System.Type type =         targetObject.GetType();
        //  PropertyInfo propertyReflected =     targetObject.GetProperty(conditionPath,

        //    System.Reflection.BindingFlags.Default|
        //  System.Reflection.BindingFlags.Instance |System.Reflection.BindingFlags.NonPublic| System.Reflection.BindingFlags.Public);


        //        var allprops = type.GetProperties();
        //     var allFilds = type.GetFields();
        //    var m = "";
        //  foreach(PropertyInfo VARIABLE in allprops) {
        //    m += VARIABLE.ToString() + "\r\n";
        // }//
        //        Debug.LogWarning(type + " :  "+ targetObject + " ["+ propertyReflected +"]\r\nALL:" +allprops);


        //  var value = propertyReflected.GetValue(targetObject);

        //        Debug.LogWarning(type + " :  "+ targetObject + " "+ propertyReflected + " "+value);
        // SerializedProperty sourcePropertyValue = propertyReflected
        // condition path is states.Array.data[1].aorBFixer
        // target object : EzSpriteAnimator Example (EzSpriteAnimator
        //SerializedProperty sourcePropertyValue = property.serializedObject.FindProperty(condHAtt.ConditionalSourceField);

        //  if (sourcePropertyValue != null)
        {
            //      enabled = CheckPropertyType(sourcePropertyValue);
        }
        //        else
        //      {
        //            Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField +"\r\nUSING : "+conditionPath + " on  "+property.serializedObject.targetObject);
        //  }
        if(condHAtt.invertResult) {
            return !enabled;
        } else {
            return enabled;
        }
    }


    private static bool CheckPropertyType(object val) {
        if(val is bool) {
            return (bool) val;
        }

        return true;
    }

    private bool CheckPropertyType(UnityEditor.SerializedProperty sourcePropertyValue) {
        T condHAtt = (T) attribute;
        switch(sourcePropertyValue.propertyType) {
            case UnityEditor.SerializedPropertyType.Boolean:
                return sourcePropertyValue.boolValue;
            case UnityEditor.SerializedPropertyType.ObjectReference:
                return sourcePropertyValue.objectReferenceValue != null;
            case UnityEditor.SerializedPropertyType.Enum:
                return condHAtt.enumIndex == sourcePropertyValue.enumValueIndex;
            default:
                Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
                return true;
        }
    }

    // todo do this more efficently!  less exceptions!

    private const System.Reflection.BindingFlags bindingFlags = System.Reflection.BindingFlags.Instance |
                                                                System.Reflection.BindingFlags.NonPublic |
                                                                System.Reflection.BindingFlags.Public;

    public static object RecursiveGetProperty(object o, string path) { //= "states.Array.data[1].aorBFixer"
        System.Type otype = o.GetType();
        if(!path.Contains(".")) {
            System.Reflection.PropertyInfo oprop = otype.GetProperty(path, bindingFlags); // show seconds per loop
            System.Reflection.PropertyInfo[] oprops = otype.GetProperties(bindingFlags); // show seconds per loop
            System.Reflection.PropertyInfo[] oprops2 = otype.GetProperties(); // show seconds per loop


            if(oprop != null) {
                return oprop.GetValue(o);
            }

            System.Reflection.FieldInfo ofield = otype.GetField(path, bindingFlags);
            if(ofield != null) {
                return ofield.GetValue(o);
            }
        } else {
            string[] splits = path.Split(new[] {'.'}, 2);

            System.Reflection.PropertyInfo op = otype.GetProperty(splits[0], bindingFlags); // show seconds per loop

            if(op != null) {
                object o1 = op.GetValue(o);
                return RecursiveGetProperty(o1, splits[1]);
            }

            System.Reflection.FieldInfo op2 = otype.GetField(splits[0], bindingFlags);
            if(op2 != null) {
                object o2 = op2.GetValue(o);
                return RecursiveGetProperty(o2, splits[1]);
            }
        }

        try {
            string TypeName = otype.GetGenericTypeDefinition().Name;
            if(TypeName == "List`1") {
                string[] splits2 = path.Split(new[] {'.', '[', ']'}, 5);
                // array, data number, rest?
                object[] index = {int.Parse(splits2[2])};
                object value = otype.GetProperty("Item", bindingFlags).GetValue(o, index);

                return RecursiveGetProperty(value, splits2[4]);
            } else {
                Debug.Log("UNKNOWN TYPE " + path);
                return null;
            }
        } catch(System.Exception e) {
            Debug.LogError("ERROR trying to test for list on " + o + "->" + path + " : " + e.Message);
            return null;
        }
    }
}
