using System.Collections.Generic;
using UnityEngine;

public class DepthSlicerPresetWindow : UnityEditor.EditorWindow {
    // Add menu named "My Window" to the Window menu
    [UnityEditor.MenuItem("Ez 2.5d/Depth Slicer Presets")]
    private static void Init() {
        RefreshMenu();
        // Get existing open window or if none, make a new one:
        DepthSlicerPresetWindow window = (DepthSlicerPresetWindow) GetWindow(typeof(DepthSlicerPresetWindow));
        window.Show();
    }

    private EzSpriteAnimator.DepthSlicerOptions currentlySelectedOptions {
        get {
            Object o = UnityEditor.Selection.activeObject;
            try {
                GameObject go = (GameObject) o;

                if(go != null) {
                    EzSpriteAnimator ez = go.GetComponent<EzSpriteAnimator>();
                    if(ez != null) {
                        return ez.spriteOptions.depthSlicerOptions;
                    }
                }
            } catch {
                Debug.Log(o);
                Object obj = UnityEditor.Selection.activeObject;
                if(obj != null) {
                    UnityEngine.Timeline.TimelineClip clip = EzReflectionExtensions.GetValue<UnityEngine.Timeline.TimelineClip>(obj, "clip");
                    EzDepthSlicerControlClip asset = (EzDepthSlicerControlClip) clip.asset;
                    return asset.template.depthSlicerOptions;
                }
            }

            return null;
        }
    }

    private static List<DepthSlicerPreset> list = null;

    private void OnGUI() {
        if(list == null) {
            RefreshMenu();
        }

        if(Event.current.type == EventType.Layout) {
            //            Debug.Log(currentlySelectedOptions);
        }

        EzSpriteAnimator.DepthSlicerOptions co = currentlySelectedOptions;
        if(co == null) {
            GUILayout.Label("Please Select a EzSpriteAnimator", UnityEditor.EditorStyles.boldLabel);
            return;
        } else {
            GUILayout.Label("Depth Slicer Presets (Click to apply)", UnityEditor.EditorStyles.boldLabel);
        }

        EzEditorGuiExtensions.HorizontalGroup(() => {
            defaultName = UnityEditor.EditorGUILayout.TextField(defaultName);
            if(System.IO.File.Exists("Assets/" + defaultName + ".asset")) {
                defaultName = defaultName + "X";
            }

            if(GUILayout.Button("New Preset")) {
                DepthSlicerPreset asset = CreateInstance<DepthSlicerPreset>();
                asset.name = defaultName;
                asset.option.ApplyOptions(co, false);
                UnityEditor.AssetDatabase.CreateAsset(asset, "Assets/" + defaultName + ".asset");
                UnityEditor.AssetDatabase.SaveAssets();
                list.Add(asset);
            }
        });


        System.Action todo = null;

        verticalScroll = UnityEditor.EditorGUILayout.BeginScrollView(verticalScroll);
        foreach(DepthSlicerPreset VARIABLE in list) {
            if(VARIABLE == null || string.IsNullOrWhiteSpace(VARIABLE.name)) {
                todo = RefreshMenu;
                break;
            }

            GUILayout.BeginHorizontal();
            if(GUILayout.Button(VARIABLE.name)) {
                co.ApplyOptions(VARIABLE.option, false);
            }

            GUILayout.EndHorizontal();
            //  UnityEditor.Editor tmpEditor = UnityEditor.Editor.CreateEditor(VARIABLE.option); tmpEditor.OnInspectorGUI();
        }

        UnityEditor.EditorGUILayout.EndScrollView();


        if(todo != null) {
            todo();
        }
        /*
                GUILayout.Label("Base Settings", UnityEditor.EditorStyles.boldLabel);
                myString = UnityEditor.EditorGUILayout.TextField("Text Field", myString);
        
                groupEnabled = UnityEditor.EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
                myBool = UnityEditor.EditorGUILayout.Toggle("Toggle", myBool);
                myFloat = UnityEditor.EditorGUILayout.Slider("Slider", myFloat, -3, 3);
                UnityEditor.EditorGUILayout.EndToggleGroup();
        */

        if(GUILayout.Button("RefreshList")) {
            RefreshMenu();
        }
    }

    private static Vector2 verticalScroll = Vector2.zero;
    private static string defaultName = "DepthSlicerDefault";

    private static void RefreshMenu() {
        list = EzScriptableObjectExtensions.GetAllInstances<DepthSlicerPreset>(
            (depthSlicerPreset, path) => { depthSlicerPreset.name = path.Replace("Assets/", "").Replace(".asset", ""); });
    }
}
