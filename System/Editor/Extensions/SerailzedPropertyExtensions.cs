using System;
using System.Linq;

public static class SerializedPropertyExtensions {
    public static object GetValue(this UnityEditor.SerializedProperty prop) {
        string path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        string[] elements = path.Split('.');
        foreach(string element in elements.Take(elements.Length)) {
            if(element.Contains("[")) {
                string elementName = element.Substring(0, element.IndexOf("["));
                int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = EzReflectionExtensions.GetValue(obj, elementName, index);
            } else {
                obj = EzReflectionExtensions.GetValue(obj, element);
            }
        }

        return obj;
    }

    public static object GetParent(this UnityEditor.SerializedProperty prop, int parentIndex = 1) {
        string path = prop.propertyPath.Replace(".Array.data[", "[");
        object obj = prop.serializedObject.targetObject;
        string[] elements = path.Split('.');
        foreach(string element in elements.Take(elements.Length - parentIndex)) {
            if(element.Contains("[")) {
                string elementName = element.Substring(0, element.IndexOf("["));
                int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                obj = EzReflectionExtensions.GetValue(obj, elementName, index);
            } else {
                obj = EzReflectionExtensions.GetValue(obj, element);
            }
        }

        return obj;
    }

    public static T GetParent<T>(this UnityEditor.SerializedProperty prop, int parentIndex = 1) {
        object obj = prop.GetParent(parentIndex);
        if(obj is T) {
            return (T) obj;
        }

        try {
            return (T) Convert.ChangeType(obj, typeof(T));
        } catch(InvalidCastException) {
            return default;
        }
    }

    /*
        static
            public object GetValue(object source, string name) {
            if(source == null)
                return null;
            var type = source.GetType();
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if(f == null) {
                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if(p == null)
                    return null;
                return p.GetValue(source, null);
            }
    
            return f.GetValue(source);
        }
    */
}
