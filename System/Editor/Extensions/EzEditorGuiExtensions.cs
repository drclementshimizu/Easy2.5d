public static class EzEditorGuiExtensions {
    public static void HorizontalGroup(System.Action a) {
        UnityEditor.EditorGUILayout.BeginHorizontal();
        a();
        UnityEditor.EditorGUILayout.EndHorizontal();
    }

    public static void VerticalGroup(System.Action a) {
        UnityEditor.EditorGUILayout.BeginVertical();
        a();
        UnityEditor.EditorGUILayout.EndVertical();
    }
}
