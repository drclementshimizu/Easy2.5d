using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[System.Serializable]
public class EzDepthSlicerControlClip : UnityEngine.Playables.PlayableAsset, UnityEngine.Timeline.ITimelineClipAsset {
    [SerializeField]
    public EzDepthSlicerControlBehavior template = new EzDepthSlicerControlBehavior();

    public UnityEngine.Timeline.ClipCaps clipCaps {
        get { return UnityEngine.Timeline.ClipCaps.Blending; }
    }

    public override UnityEngine.Playables.Playable CreatePlayable(UnityEngine.Playables.PlayableGraph graph, GameObject owner) {
        return UnityEngine.Playables.ScriptPlayable<EzDepthSlicerControlBehavior>.Create(graph, template);
    }
}
