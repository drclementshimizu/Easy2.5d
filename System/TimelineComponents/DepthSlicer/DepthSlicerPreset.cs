using UnityEngine;

public class DepthSlicerPreset : ScriptableObject {
    public EzSpriteAnimator.DepthSlicerOptions option = new EzSpriteAnimator.DepthSlicerOptions {enabled = true};
}
