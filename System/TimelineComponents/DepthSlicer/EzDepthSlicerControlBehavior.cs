using System.Collections.Generic;
using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


[System.Serializable]
public class EzDepthSlicerControlBehavior : UnityEngine.Playables.PlayableBehaviour {
    public DepthSlicerOptionsControl depthSlicerOptions = new DepthSlicerOptionsControl {enabled = true};
    private DepthSlicerOptionsControl temp = new DepthSlicerOptionsControl {enabled = true};

    [System.Serializable]
    public class DepthSlicerOptionsControl : EzSpriteAnimator.DepthSlicerOptions {
        public override bool isInAnimationEditor {
            get { return true; }
        }

        public override bool showTransformOptions {
            get { return true; }
        }
    }

    private Dictionary<EzSpriteAnimator.DepthSlicerOptions.MODE, float> enumCounter = new Dictionary<EzSpriteAnimator.DepthSlicerOptions.MODE, float>();

    public override void ProcessFrame(UnityEngine.Playables.Playable playable, UnityEngine.Playables.FrameData info, object playerData) {
        EzSpriteAnimator target = playerData as EzSpriteAnimator;
        if(target == null) {
            return;
        }

        enumCounter.Clear();
        temp.pivot.localPosition = Vector3.zero;
        temp.pivot.localEulerAngles = Vector3.zero;
        temp.pivot.localScale = Vector3.zero;
        temp.offset.localPosition = Vector3.zero;
        temp.offset.localEulerAngles = Vector3.zero;
        temp.offset.localScale = Vector3.zero;
        float fcount = 0;
        int inputCount = UnityEngine.Playables.PlayableExtensions.GetInputCount(playable);
        float weightSum = 0;
        float max = 0;
        EzSpriteAnimator.DepthSlicerOptions.MODE bestMode = EzSpriteAnimator.DepthSlicerOptions.MODE.OffsetTimesIndex;
        for(int i = 0; i < inputCount; i++) {
            float inputWeight = UnityEngine.Playables.PlayableExtensions.GetInputWeight(playable, i);
            weightSum += inputWeight;
            UnityEngine.Playables.ScriptPlayable<EzDepthSlicerControlBehavior> inputPlayable = (UnityEngine.Playables.ScriptPlayable<EzDepthSlicerControlBehavior>) UnityEngine.Playables.PlayableExtensions.GetInput(playable, i);
            EzDepthSlicerControlBehavior input = inputPlayable.GetBehaviour();

            temp.pivot.localPosition += input.depthSlicerOptions.pivot.localPosition * inputWeight;
            temp.pivot.localEulerAngles += input.depthSlicerOptions.pivot.localEulerAngles * inputWeight;
            temp.pivot.localScale += input.depthSlicerOptions.pivot.localScale * inputWeight;
            temp.offset.localPosition += input.depthSlicerOptions.offset.localPosition * inputWeight;
            temp.offset.localEulerAngles += input.depthSlicerOptions.offset.localEulerAngles * inputWeight;
            temp.offset.localScale += input.depthSlicerOptions.offset.localScale * inputWeight;
            fcount += input.depthSlicerOptions.advancedOptions.count * inputWeight;


            float val = inputWeight;
            if(!enumCounter.ContainsKey(input.depthSlicerOptions.advancedOptions.mode)
            ) {
                enumCounter.Add(input.depthSlicerOptions.advancedOptions.mode, inputWeight);
            } else {
                val += enumCounter[input.depthSlicerOptions.advancedOptions.mode];
                enumCounter[input.depthSlicerOptions.advancedOptions.mode] = val;
            }

            if(max < val) {
                max = val;
                bestMode = input.depthSlicerOptions.advancedOptions.mode;
            }
        }

        if(weightSum > .5f) {
            temp.pivot.localPosition /= weightSum;
            temp.pivot.localEulerAngles /= weightSum;
            temp.pivot.localScale /= weightSum;
            temp.offset.localPosition /= weightSum;
            temp.offset.localEulerAngles /= weightSum;
            temp.offset.localScale /= weightSum;
            temp.advancedOptions.count = Mathf.RoundToInt(Mathf.Max(3f, fcount / weightSum));
            temp.advancedOptions.mode = bestMode;
            target.spriteOptions.depthSlicerOptions.ApplyOptions(temp, true);
        }
    }
}
