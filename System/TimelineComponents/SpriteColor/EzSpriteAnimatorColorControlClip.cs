using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[System.Serializable]
public class EzSpriteAnimatorColorControlClip : UnityEngine.Playables.PlayableAsset, UnityEngine.Timeline.ITimelineClipAsset {
    [SerializeField]
    private EzSpriteAnimatorColorControlBehavior template = new EzSpriteAnimatorColorControlBehavior();

    public UnityEngine.Timeline.ClipCaps clipCaps {
        get { return UnityEngine.Timeline.ClipCaps.Blending; }
    }

    public override UnityEngine.Playables.Playable CreatePlayable(UnityEngine.Playables.PlayableGraph graph, GameObject owner) {
        return UnityEngine.Playables.ScriptPlayable<EzSpriteAnimatorColorControlBehavior>.Create(graph, template);
    }
}
