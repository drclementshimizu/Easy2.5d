﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[UnityEngine.Timeline.TrackColor(241f / 255f, 249f / 255, 241f / 255f)]
[UnityEngine.Timeline.TrackBindingType(typeof(EzSpriteAnimator))]
[UnityEngine.Timeline.TrackClipType(typeof(EzSpriteAnimatorColorControlClip))]
public class EzSpriteAnimatorColorControlTrack : UnityEngine.Timeline.TrackAsset {
    public override UnityEngine.Playables.Playable CreateTrackMixer(UnityEngine.Playables.PlayableGraph graph, GameObject go, int inputCount) {
        UnityEngine.Playables.ScriptPlayable<EzSpriteAnimatorColorControlBehavior> scriptPlayable = UnityEngine.Playables.ScriptPlayable<EzSpriteAnimatorColorControlBehavior>.Create(graph, inputCount);
        return scriptPlayable;
    }
}
