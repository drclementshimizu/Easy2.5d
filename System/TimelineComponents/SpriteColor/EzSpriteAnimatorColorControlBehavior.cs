using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


[System.Serializable]
public class EzSpriteAnimatorColorControlBehavior : UnityEngine.Playables.PlayableBehaviour {
    [Header("We suggest you limit your control points to time 0, .25, .5, .75, and 1.0")]
    [Header("Animating this effect is expensive!  Use sparingly!")]
    public _ColorOptionsControl colorOptions = new _ColorOptionsControl();

    private _ColorOptionsControl temp = new _ColorOptionsControl {color = new Gradient {colorKeys = new GradientColorKey[5], alphaKeys = new GradientAlphaKey[5]}};

    [System.Serializable]
    public class _ColorOptionsControl : EzSpriteAnimator.SpriteOptions.ColorOptions {
        public override bool isInAnimationEditor {
            get { return true; }
        }
    }

    private GradientColorKey[] colorKeys = new GradientColorKey[5];
    private GradientAlphaKey[] alphaKeys = new GradientAlphaKey[5];

    public override void ProcessFrame(UnityEngine.Playables.Playable playable, UnityEngine.Playables.FrameData info, object playerData) {
        EzSpriteAnimator target = playerData as EzSpriteAnimator;
        if(target == null) {
            return;
        }

        for(int i = 0; i < 5; i++) {
            colorKeys[i].time = i / (5.0f - 1f);
            alphaKeys[i].time = i / (5.0f - 1f);
            colorKeys[i].color = Color.black;
            alphaKeys[i].alpha = 0;
        }

        temp.secondsPerLoop = 0;
        int inputCount = UnityEngine.Playables.PlayableExtensions.GetInputCount(playable);
        float weightSum = 0;
        for(int i = 0; i < inputCount; i++) {
            float inputWeight = UnityEngine.Playables.PlayableExtensions.GetInputWeight(playable, i);
            weightSum += inputWeight;
            UnityEngine.Playables.ScriptPlayable<EzSpriteAnimatorColorControlBehavior> inputPlayable = (UnityEngine.Playables.ScriptPlayable<EzSpriteAnimatorColorControlBehavior>) UnityEngine.Playables.PlayableExtensions.GetInput(playable, i);
            EzSpriteAnimatorColorControlBehavior input = inputPlayable.GetBehaviour();

            for(int splineIndex = 0; splineIndex < 5; splineIndex++) {
                float t = splineIndex / (5.0f - 1f);
                Color color = input.colorOptions.color.Evaluate(t);
                colorKeys[splineIndex].color = new Color(
                    colorKeys[splineIndex].color.r + color.r * inputWeight,
                    colorKeys[splineIndex].color.g + color.g * inputWeight,
                    colorKeys[splineIndex].color.b + color.b * inputWeight);
                alphaKeys[splineIndex].alpha = alphaKeys[splineIndex].alpha + color.a * inputWeight;
            }

            temp.secondsPerLoop += input.colorOptions.secondsPerLoop * inputWeight;
        }

        if(weightSum > .5f) {
            target.spriteOptions.colorOptions.color.SetKeys(colorKeys, alphaKeys);
            target.spriteOptions.colorOptions.secondsPerLoop = temp.secondsPerLoop;
        }
    }
}
