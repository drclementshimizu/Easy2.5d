using UnityEngine;

public static class EzScriptableObjectExtensions {
    //https://answers.unity.com/questions/1425758/how-can-i-find-all-instances-of-a-scriptable-objec.html
    public static System.Collections.Generic.List<T> GetAllInstances<T>(System.Action<T, string> onLoad = null) where T : ScriptableObject {
        string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(T).Name); //FindAssets uses tags check documentation for more info
        System.Collections.Generic.List<T> a = new System.Collections.Generic.List<T>();
        for(int i = 0; i < guids.Length; i++) {
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
            a.Add(asset);
            if(onLoad != null) {
                onLoad.Invoke(asset, path);
            }
        }

        return a;
    }
}
