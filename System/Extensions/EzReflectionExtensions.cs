using System;
using System.Collections;
using System.Reflection;

public static class EzReflectionExtensions {
    static
        public object GetValue(object source, string name, int index) {
        IEnumerable enumerable = GetValue(source, name) as IEnumerable;
        IEnumerator enm = enumerable.GetEnumerator();
        while(index-- >= 0) {
            enm.MoveNext();
        }

        return enm.Current;
    }

    public static T GetValue<T>(object source, string name) {
        object obj = GetValue(source, name);

        if(obj is T) {
            return (T) obj;
        }

        try {
            return (T) Convert.ChangeType(obj, typeof(T));
        } catch(InvalidCastException) {
            return default;
        }
    }

    public static object GetValue(object source, string name) {
        if(source == null) {
            return null;
        }

        Type type = source.GetType();

        FieldInfo f = FindFieldInTypeHierarchy(type, name);

        if(f == null) {
            PropertyInfo p = FindPropertyInTypeHierarchy(type, name);
            if(p == null) {
                return null;
            }

            return p.GetValue(source, null);
        }

        return f.GetValue(source);
    }

    private static FieldInfo FindFieldInTypeHierarchy(Type providedType, string fieldName) {
        FieldInfo field = providedType.GetField(fieldName, (BindingFlags) (-1));


        while(field == null && providedType.BaseType != null) {
            providedType = providedType.BaseType;
            field = providedType.GetField(fieldName, (BindingFlags) (-1));
        }

        return field;
    }

    private static PropertyInfo FindPropertyInTypeHierarchy(Type providedType, string propertyName) {
        PropertyInfo property = providedType.GetProperty(propertyName, (BindingFlags) (-1));


        while(property == null && providedType.BaseType != null) {
            providedType = providedType.BaseType;
            property = providedType.GetProperty(propertyName, (BindingFlags) (-1));
        }

        return property;
    }
}
