﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[RequireComponent(typeof(Collider))]
public class EzTriggerEvent : EzEventPicky {
    public EzEventHandler onTriggerEnter = new EzEventHandler {debugMessage = "onTriggerEnter"};

    protected void OnTriggerEnter(Collider other) {
        TryApplyEvent(onTriggerEnter, other.gameObject);
    }

#if ENABLE_SPAMMY_EVENTS
    public EasyEventHandler onTriggerStay = new EasyEventHandler {debugMessage = "onTriggerStay"};

    protected void OnTriggerStay(Collider other) {       
        TryApplyEvent(onTriggerStay,other.gameObject);
    }
#endif

    public EzEventHandler onTriggerExit = new EzEventHandler {debugMessage = "onTriggerExit"};

    protected void OnTriggerExit(Collider other) {
        TryApplyEvent(onTriggerExit, other.gameObject);
    }
}
