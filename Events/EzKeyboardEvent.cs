using System.Collections.Generic;
using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzKeyboardEvent : EzEventBase {
    public List<EzKeyPressEventHandler> events = new List<EzKeyPressEventHandler>();


    public void Start() {
        if(options.targetTransform == null && Camera.main != null) {
            options.targetTransform = Camera.main.transform;
        }
    }

    public void Update() {
        bool apply = true;
        if(options.targetMustBeNear) {
            apply = options.targetTransform && Vector3.Distance(transform.position, options.targetTransform.transform.position) < options.near;
        }

        if(apply) {
            for(int i = 0; i < events.Count; i++) {
                if(events[i].active) {
                    ApplyEvent(events[i]);
                }
            }
        }
    }

    [System.Serializable]
    public class EzKeyPressEventHandler : EzEventHandler {
        public KeyCode keyCode = KeyCode.Space;

        public bool active {
            get { return Input.GetKeyDown(keyCode); }
        }
    }

    public Options options = new Options();

    [System.Serializable]
    public class Options {
        public bool targetMustBeNear = false;
        public float near = 4;

        [Header("This defaults to the main camera.  No need to connect it.")]
        public Transform targetTransform = null;
    }
}
