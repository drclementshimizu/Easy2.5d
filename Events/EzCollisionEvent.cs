﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

[RequireComponent(typeof(Collider))]
public class EzCollisionEvent : EzEventPicky {
    public EzEventHandler onCollisionEnter = new EzEventHandler {debugMessage = "onCollisionEnter"};
    private void OnCollisionEnter(Collision other) {
        TryApplyEvent(onCollisionEnter, other.gameObject);
    }
#if ENABLE_SPAMMY_EVENTS
    public EasyEventHandler onCollisionStay = new EasyEventHandler {debugMessage = "onCollisionStay"};
    private void OnCollisionStay(Collision other) {
        TryApplyEvent(onCollisionStay,other.gameObject);
   
    }
#endif
    public EzEventHandler onCollisionExit = new EzEventHandler {debugMessage = "onCollisionExit"};

    private void OnCollisionExit(Collision other) {
        TryApplyEvent(onCollisionExit, other.gameObject);
    }
}
