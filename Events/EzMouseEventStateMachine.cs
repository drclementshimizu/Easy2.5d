﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


[RequireComponent(typeof(EZAnimatorBase))]
[RequireComponent(typeof(Collider))]
public class EzMouseEventStateMachine : EzEventBase {
    public string stateName {
        get { return options.stateName; }
        set { options.stateName = value; }
    }

    public void OnMouseDown() {
        ApplyEvent(state.onMouseDown);
    }


    public void OnMouseUp() {
        ApplyEvent(state.onMouseUp);
    }


    public System.Collections.Generic.List<State> states = new System.Collections.Generic.List<State>();
    public State fallbackState = new State();

    [System.Serializable]
    public class State {
        public string name;

        public EzEventHandler onMouseUp = new EzEventHandler {debugMessage = "onMouseUp"};
        public EzEventHandler onMouseDown = new EzEventHandler {debugMessage = "onMouseDown"};
    }

    public override void SetNewState(string newStateName) {
        if(options.copyStateNameFromTargetAnimator && target) {
            stateName = target.stateName;
        } else {
            stateName = newStateName;
        }
    }

    [System.Serializable]
    public class Options {
        public bool copyStateNameFromTargetAnimator = true;
        public string stateName;
    }

    public Options options = new Options();

    public State state {
        get {
            if(options.copyStateNameFromTargetAnimator && target) {
                stateName = target.stateName;
            }

            for(int i = 0; i < states.Count; i++) {
                if(states[i].name == stateName) {
                    return states[i];
                }
            }

            return fallbackState;
        }
    }
}
