﻿using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.Button))]
public class EzUiButtonEventStateMachine : EzEventBase {
    public string stateName {
        get { return options.stateName; }
        set { options.stateName = value; }
    }

    private void OnEnable() {
        UnityEngine.UI.Button b = GetComponent<UnityEngine.UI.Button>();
        b.onClick.AddListener(OnClick);
    }

    private void OnClick() {
        ApplyEvent(state.onClick);
    }

    private void OnDisable() {
        UnityEngine.UI.Button b = GetComponent<UnityEngine.UI.Button>();
        b.onClick.RemoveListener(OnClick);
    }

    public System.Collections.Generic.List<State> states = new System.Collections.Generic.List<State>();
    public State fallbackState = new State();

    [System.Serializable]
    public class State {
        public string name;
        public EzEventHandler onClick = new EzEventHandler {debugMessage = "onClick"};
    }

    [System.Serializable]
    public class Options {
        public bool copyStateNameFromTargetAnimator = true;
        public string stateName;
    }

    public override void SetNewState(string newStateName) {
        if(options.copyStateNameFromTargetAnimator && target) {
            stateName = target.stateName;
        } else {
            stateName = newStateName;
        }
    }


    public Options options = new Options();

    private State state {
        get {
            if(options.copyStateNameFromTargetAnimator && target) {
                stateName = target.stateName;
            }

            for(int i = 0; i < states.Count; i++) {
                if(states[i].name == stateName) {
                    return states[i];
                }
            }

            return fallbackState;
        }
    }
}
