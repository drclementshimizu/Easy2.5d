using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzProximityEvent : EzEventBase {
    public EzEventHandler onNear = new EzEventHandler {debugMessage = "onNear"};
    public EzEventHandler onFar = new EzEventHandler {debugMessage = "onFar"};

    public void Start() {
        if(options.targetTransform == null && Camera.main != null) {
            options.targetTransform = Camera.main.transform;
        }
    }

    private enum State {
        far,
        near,
        awake
    }

    private State state = State.awake;

    [System.Serializable]
    public class Options {
        [Header("visualized by the green ring")]
        [Header("onNear will get triggered when the target gets closer then near")]
        public float near = 4;

        [Header("visualized by the red ring")]
        [Header("onFar will get triggered when the target gets farther then far")]
        public float far = 5;

        public bool ignoreEventOnAwake = true;

        [Header("This defaults to the main camera.  No need to connect it.")]
        public Transform targetTransform = null;
    }

    public Options options = new Options();


    public void Update() {
        if(options.targetTransform) {
            float thisDis = Vector3.Distance(transform.position, options.targetTransform.transform.position);
            if(thisDis < options.near) {
                if(state == State.far || state == State.awake && !options.ignoreEventOnAwake) {
                    ApplyEvent(onNear);
                }

                state = State.near;
            } else if(thisDis > options.far) {
                if(state == State.near || state == State.awake && !options.ignoreEventOnAwake) {
                    ApplyEvent(onFar);
                }

                state = State.far;
            }
        }
    }

    private void OnDrawGizmosSelected() {
        if(options.near < 0) {
            options.near = 0;
        }

        if(options.far < options.near) {
            options.far = options.near;
        }

        Color push = Gizmos.color;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, options.far);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, options.near);
        Gizmos.color = push;
    }
}
