﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


[RequireComponent(typeof(Collider))]
public class EzMouseEvent : EzEventBase {
    public EzEventHandler onMouseDown = new EzEventHandler {debugMessage = "onMouseDown"};

    public void OnMouseDown() {
        ApplyEvent(onMouseDown);
    }

    public EzEventHandler onMouseUp = new EzEventHandler {debugMessage = "onMouseUp"};

    public void OnMouseUp() {
        ApplyEvent(onMouseUp);
    }


    public EzEventHandler onMouseEnter = new EzEventHandler {debugMessage = "onMouseEnter"};

    public void OnMouseEnter() {
        ApplyEvent(onMouseEnter);
    }
#if ENABLE_SPAMMY_EVENTS
    public EasyEventHandler onMouseOver = new EasyEventHandler {debugMessage = "onMouseOver"};   
        private void OnMouseOver() {     
        ApplyEvent(onMouseOver);

    }
#endif
    public EzEventHandler onMouseExit = new EzEventHandler {debugMessage = "onMouseExit"};

    public void OnMouseExit() {
        ApplyEvent(onMouseExit);
    }
}
