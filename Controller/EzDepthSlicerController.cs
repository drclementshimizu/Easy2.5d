﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzDepthSlicerController : MonoBehaviour {
    private EzSpriteAnimator ezSpriteAnimator;
    private SpriteRenderer sr;

    private void Awake() {
        ezSpriteAnimator = GetComponent<EzSpriteAnimator>();
        sr = GetComponent<SpriteRenderer>();
    }

    public void SetCount(float count) {
        SetCount((int) count);
    }

    public void SetCount(int count) {
        if(ezSpriteAnimator) {
            if(count < 3) {
                ezSpriteAnimator.spriteOptions.depthSlicerOptions.enabled = false;
                sr.enabled = true;
            } else {
                sr.enabled = false;
                ezSpriteAnimator.spriteOptions.depthSlicerOptions.enabled = true;
                ezSpriteAnimator.spriteOptions.depthSlicerOptions.advancedOptions.count = count;
                ezSpriteAnimator.spriteOptions.depthSlicerOptions.positionDirty = true;
                ezSpriteAnimator.BootstrapDepthSlicer();
            }
        }
    }

    public void FrameOffsetNone() {
        if(ezSpriteAnimator) {
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.advancedOptions.frameOffset = EzSpriteAnimator.DepthSlicerOptions.OFFSETMODE.NONE;
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.positionDirty = true;
            ezSpriteAnimator.BootstrapDepthSlicer();
        }
    }

    public void FrameOffsetOne() {
        if(ezSpriteAnimator) {
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.advancedOptions.frameOffset = EzSpriteAnimator.DepthSlicerOptions.OFFSETMODE.ONE;
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.positionDirty = true;
            ezSpriteAnimator.BootstrapDepthSlicer();
        }
    }

    public void FrameOffsetNegativeOne() {
        if(ezSpriteAnimator) {
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.advancedOptions.frameOffset = EzSpriteAnimator.DepthSlicerOptions.OFFSETMODE.NEGATVIEONE;
            ezSpriteAnimator.spriteOptions.depthSlicerOptions.positionDirty = true;
            ezSpriteAnimator.BootstrapDepthSlicer();
        }
    }
}
