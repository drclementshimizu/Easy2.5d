﻿using UnityEngine;

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////

public class EzTimelineController : MonoBehaviour {
    private UnityEngine.Playables.PlayableDirector director;

    private void Awake() {
        director = GetComponent<UnityEngine.Playables.PlayableDirector>();
    }


    public void Play() {
        if(director) {
            director.Play();
        }
    }

    public void Stop() {
        if(director) {
            director.Stop();
        }
    }

    public void Pause() {
        if(director) {
            director.Pause();
        }
    }


    public float time {
        get {
            if(director) {
                return (float) director.time;
            }

            return 0;
        }
        set {
            if(director) {
                director.time = value;
                director.Evaluate();
            }
        }
    }
}
