﻿

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzGameObjectController : EzEventBase {
    public void ToggleGameObject() {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
