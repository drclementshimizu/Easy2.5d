﻿

///////////////////////////////////////////////////////
// Easy 2.5D
// By Dr. Clement Shimizu
// https://gitlab.com/drclementshimizu/Easy2.5d
// twitter: @DrClementShimizu
// http://www.clementshimizu.com/
///////////////////////////////////////////////////////


public class EzTransformController : EzEventBase {
    public void SetPositionLocalX(float val) {
        transform.localPosition = transform.localPosition.SetX(val);
    }

    public void SetPositionLocalY(float val) {
        transform.localPosition = transform.localPosition.SetY(val);
    }

    public void SetPositionLocalZ(float val) {
        transform.localPosition = transform.localPosition.SetZ(val);
    }

    public void AddPositionLocalX(float val) {
        transform.localPosition = transform.localPosition.AddX(val);
    }

    public void AddPositionLocalY(float val) {
        transform.localPosition = transform.localPosition.AddY(val);
    }

    public void AddPositionLocalZ(float val) {
        transform.localPosition = transform.localPosition.AddZ(val);
    }

    public void SetPositionGlobalX(float val) {
        transform.position = transform.position.SetX(val);
    }

    public void SetPositionGlobalY(float val) {
        transform.position = transform.position.SetY(val);
    }

    public void SetPositionGlobalZ(float val) {
        transform.position = transform.position.AddZ(val);
    }

    public void AddPositionGlobalX(float val) {
        transform.position = transform.position.AddX(val);
    }

    public void AddPositionGlobalY(float val) {
        transform.position = transform.position.AddY(val);
    }

    public void AddPositionGlobalZ(float val) {
        transform.position = transform.position.AddZ(val);
    }
}
